﻿#define _USE_MATH_DEFINES
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include <locale.h>
using namespace std;


int const N = 150;	// liczba krokow
int const liczba_wiezlow = 15; // liczba wezlow
// podany w zadaniu przedzial:
double const a = -1.0, b = 1.0; 
double const dx = (b - a) / (liczba_wiezlow - 1);	//skok dla liczby wezlow
double const dN = (b - a) / N;	//skok dla liczby krokow

double Newton(double x, double* xi, double* yi) {
	vector<double> iloczyn(liczba_wiezlow + 1);
	vector<double> c(liczba_wiezlow);
	 
	double wynik = 0.0; 

	// przypisuje wartosci wspolczynniku:
	for (int i = 0; i < liczba_wiezlow; i++) {
		c[i] = yi[0]; 

		for (int j = 0; j < liczba_wiezlow - i - 1; j++)
			yi[j] = (yi[j + 1] - yi[j]) / (xi[i + j + 1] - xi[j]); 
	}

	// tworze wielomiany bazowe:
	iloczyn[0] = 1.0;
	for (int i = 1; i < liczba_wiezlow + 1; i++)
		iloczyn[i] = iloczyn[i - 1] * (x - xi[i - 1]); 

	for (int i = 0; i < liczba_wiezlow; i++)
		wynik += c[i] * iloczyn[i]; 

	return wynik;
}

double f(double x)
{
	return x / (1.0 + 20.0 * pow(x, 4));
}

double Czebyszew(double x)
{
	double xCz[liczba_wiezlow];
	double yCz[liczba_wiezlow];

	for (int i = 0; i < liczba_wiezlow; i++)
	{
		xCz[i] = ((b + a) / 2.0) + ((b - a) / 2.0) * cos(((2.0 * i + 1.0) * M_PI) / (2.0 * (liczba_wiezlow - 1) + 2)); // wyznaczanie kolejnego wezla
		yCz[i] = f(xCz[i]); // wyliczanie funkckji dla wezla
	}
	return Newton(x, xCz, yCz);
}

double rownoodlegle(double x)
{
	double xi[liczba_wiezlow];
	double yi[liczba_wiezlow];

	int i = 0;

	for (double j = a; j <= b; j += dx)
	{
		xi[i] = j; // poczatek przedzialu
		yi[i] = f(j); // wyliczenie funkcji dla wezla 
		i++;
	}
	return Newton(x, xi, yi);
}

int main()
{
	ofstream plik("Wyniki_MO12.csv", ofstream::trunc);
	locale mylocale("");
	plik.imbue(mylocale);

	double x = a;
	for (int i = 0; i < N + 1; i++)
	{
		plik << x << " " << f(x) << " " << Czebyszew(x) << " " << rownoodlegle(x) << endl;
		x += dN;
	}
	plik.close();

	cout << "Wszystko zostalo zapisane do pliku!" << endl;
	return 0;
}

