#pragma once

#include "EquationsSolver.h"

template <std::size_t N>
class SOREquationsSolver : public EquationsSolver<N>
{
public:
    SOREquationsSolver(const std::array<double, N>& start,
                          const std::array<double, N* N>& matrix,
                          const std::array<double, N>& constantTerms,
                          unsigned int maxIterations,
                          double omega,
                          double tolerance = MACHINE_EPSILON)
        : EquationsSolver<N>(start, matrix, constantTerms, maxIterations, tolerance), m_omega(omega)
    {
        m_currentApproximation = *(this->m_startingValues);
        m_LDmatrix = *(this->m_coefficientsMatrix);
        m_DUmatrix = *(this->m_coefficientsMatrix);
        decomposeMatrix();
    }

    virtual const std::array<double, N>& getResult() override
    {
        return m_currentApproximation;
    }

    virtual const std::array<double, N>& calculate() override
    {
        ES_LOG_HEADER("METODA SOR");

        unsigned int iterationNum = 0;
        double residuum = 0.0;
        double errorEstimator = 0.0;
        //tablica przechowywujaca przyblizenie rozwiazania z wczesniejszej iteracji
        std::array<double, N> previousSolutionApproximation = m_currentApproximation;

        do
        {
            iterationNum++;

            //poprzednie przyblizenie = aktualne przeblizenie z poprzedniego etapu
            previousSolutionApproximation = m_currentApproximation;

            calculateNextApproximation(previousSolutionApproximation);
            errorEstimator = calculateErrorEstimator(previousSolutionApproximation);
            residuum = calculateResiduum();

        #ifdef ES_PRINT_STEPS
            std::cout << "Iteracja #" << iterationNum << "\nXn = ";
            MVutil::vector::print(m_currentApproximation);
            std::cout << "\nEstymator bledu: " << errorEstimator << " residuum: " << residuum << "\n";
            std::cout << std::endl;
        #endif // ES_PRINT_STEPS

        } while (iterationNum < this->MAX_ITERATIONS &&
            (residuum > this->TOLERANCE || errorEstimator > this->TOLERANCE));

        return m_currentApproximation;
    }


private:
    /**
     * rozklad macierzy wspolczynnikow na dwie macierze: L+D oraz D+U
     * D zmodyfikowana parametrem omega
     */
    virtual void decomposeMatrix() override
    {
        for (size_t i = 0; i < N; i++)
        {
            for (size_t j = 0; j < N; j++)
            {
                if (i < j)
                {
                    m_LDmatrix[i * N + j] = 0.0;
                }
                else if (i == j)
                {
                    m_LDmatrix[i * N + j] *= 1.0 / m_omega;
                    m_DUmatrix[i * N + j] *= 1.0 - (1.0 / m_omega);
                }
                else
                {
                    m_DUmatrix[i * N + j] = 0.0;
                }
            }
        }
    }

    virtual inline void calculateNextApproximation(const std::array<double, N>& prevApproximation) override
    {
        std::array<double, N> tmpConstantTermsVec;

        //obliczenie wektora z prawej strony rownania LDx = -DUx + b
        for (size_t i = 0; i < N; i++)
        {
            double sumInRow = 0.0;
            for (size_t j = 0; j < N; j++)
            {
                sumInRow += m_DUmatrix[i * N + j] * prevApproximation[j];
            }
            tmpConstantTermsVec[i] = this->m_constantTermsVector->at(i) - sumInRow;
        }

        //rozwiazanie ukladu rownan z macierza trojkatna dolna
        m_currentApproximation[0] = tmpConstantTermsVec[0] / m_LDmatrix[0];
        for (int i = 1; i < N; i++)
        {
            double sum = 0.0;
            for (int j = 0; j < i; j++)
            {
                sum += m_LDmatrix[i * N + j] * m_currentApproximation[j];
            }
            m_currentApproximation[i] = (tmpConstantTermsVec[i] - sum) / m_LDmatrix[i * N + i];
        }
    }

    virtual double calculateErrorEstimator(const std::array<double, N> & prevApproximation) override
    {
        double max = std::abs(m_currentApproximation[0] - prevApproximation[0]);
        double tmpVal;

        for (size_t i = 1; i < N; i++)
        {
            tmpVal = std::abs(m_currentApproximation[i] - prevApproximation[i]);
            if (tmpVal > max)
                max = tmpVal;
        }

        return max;
    }

    virtual double calculateResiduum() override
    {
        double tmpVal;
        double max = 0.0;
        double sumInRow = 0.0;

        for (size_t i = 0; i < N; i++)
        {
            sumInRow = 0.0;
            for (size_t j = 0; j < N; j++)
            {
                sumInRow += this->m_coefficientsMatrix->at(i * N + j) * m_currentApproximation[j];
            }
            tmpVal = std::abs(sumInRow - this->m_constantTermsVector->at(i));
            if (tmpVal > max)
                max = tmpVal;
        }

        return max;
    }

    const double m_omega;
    
    //macierz trojkatna dolna wraz z przekatna glowna przemnozona przez 1/omega
    std::array<double, N* N> m_LDmatrix;

    //macierz trojkatna gorna wraz z przekatna glowna przemnozona przez 1 - (1/omega)
    std::array<double, N* N> m_DUmatrix;

    //wektor rozwiazania ukladu
    std::array<double, N> m_currentApproximation;
};
