#include "EquationsSolver.h"
#include "JacobiEquationsSolver.h"
#include "GaussEquationsSolver.h"
#include "SOREquationsSolver.h"


template <std::size_t N>
void runSolver(EquationsSolver<N>* solver)
{
    solver->calculate();
}


int main()
{
    const std::size_t NUM_OF_DIMS = 4;

    std::array<double, NUM_OF_DIMS * NUM_OF_DIMS> coefficientsMatrix = {
         100.0, 1.0,   -2.0,   3.0,
         4.0,   300.0, -5.0,   6.0,
         7.0,  -8.0,    400.0, 9.0,
        -10.0,  11,    -12,    200
    };
    std::array<double, NUM_OF_DIMS> startingValues = { 1.0, 1.0, 1.0, 1.0 };
    std::array<double, NUM_OF_DIMS> constantTermsVector = { 395.0, 603.0, -415.0, -606.0 };

    JacobiEquationsSolver<NUM_OF_DIMS> jacobi(startingValues, coefficientsMatrix, constantTermsVector, 100);
    GaussEquationsSolver<NUM_OF_DIMS> gauss(startingValues, coefficientsMatrix, constantTermsVector, 100);
<<<<<<< Updated upstream
    SOREquationsSolver<NUM_OF_DIMS> sor(startingValues, coefficientsMatrix, constantTermsVector, 100, 0.5, 1e-7);
=======
    SOREquationsSolver<NUM_OF_DIMS> sor(startingValues, coefficientsMatrix, constantTermsVector, 100, 0.5);
>>>>>>> Stashed changes
    
    runSolver<NUM_OF_DIMS>(&gauss);
    //jacobi.calculate();
    //gauss.calculate();
    //sor.calculate();

    return 0;
}
