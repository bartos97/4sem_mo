#pragma once
#include <math.h>
#include <array>
#include "MatrixVectorUtilities.h"

// ifdef to beda wypisywane posrednie rezultaty obliczen
// ifndef to tylko zwracany wynik
#define ES_PRINT_STEPS

#define MACHINE_EPSILON 2.2204460492503131e-16

#ifdef ES_PRINT_STEPS
    #define ES_LOG_HEADER(x) {\
        printf(\
            "---------------------------------------\n"\
            "------ %s ------\n"\
            "---------------------------------------\n", \
            x\
        );\
    }
#else
    #define ES_LOG_HEADER(x);
#endif // ES_PRINT_STEPS



/**
 * Klasa bazowa dla wszystkich klas implementujacych konkretne metody iteracyjne
 * rozwiazania ukladu rownan algebraicznego liniowych
 * @template param N - ilosc rownan w ukladzie; rowne ilosci zmiennych 
 */
template <std::size_t N>
class EquationsSolver
{
public:
    virtual const std::array<double, N>& calculate() = 0;
    virtual const std::array<double, N>& getResult() = 0;

protected:
    EquationsSolver(const std::array<double, N>& start,
                    const std::array<double, N * N>& matrix,
                    const std::array<double, N>& constantTerms,
                    unsigned int maxIterations,
                    double tolerance)
        : m_startingValues(&start), m_coefficientsMatrix(&matrix), m_constantTermsVector(&constantTerms),
          MAX_ITERATIONS(maxIterations), TOLERANCE(tolerance)
    {}

    const unsigned int MAX_ITERATIONS;
    const double TOLERANCE;
    
    const std::array<double, N>* m_startingValues;
    const std::array<double, N * N>* m_coefficientsMatrix;
    const std::array<double, N>* m_constantTermsVector;

    virtual void decomposeMatrix() = 0;
    virtual void calculateNextApproximation(const std::array<double, N>& prevApproximation) = 0;
    virtual double calculateErrorEstimator(const std::array<double, N>& prevApproximation) = 0;
    virtual double calculateResiduum() = 0;
};