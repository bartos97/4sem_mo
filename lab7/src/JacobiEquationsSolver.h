#pragma once

#include "EquationsSolver.h"

template <std::size_t N>
class JacobiEquationsSolver : public EquationsSolver<N>
{
public:
    JacobiEquationsSolver(const std::array<double, N>& start,
                          const std::array<double, N * N>& matrix,
                          const std::array<double, N>& constantTerms,
                          unsigned int maxIterations,
                          double tolerance = MACHINE_EPSILON)
        : EquationsSolver<N>(start, matrix, constantTerms, maxIterations, tolerance)
    {
        m_diagonalVector = { 0.0 };
        m_LUmatrix = *(this->m_coefficientsMatrix);
        m_currentApproximation = *(this->m_startingValues);
        decomposeMatrix();
    }

    virtual const std::array<double, N>& getResult() override
    {
        return m_currentApproximation;
    }

    virtual const std::array<double, N>& calculate() override
    {
        ES_LOG_HEADER("METODA JAKOBIEGO");
        
        unsigned int iterationNum = 0;
        double residuum = 0.0;
        double errorEstimator = 0.0;
        //tablica przechowywujaca przyblizenie rozwiazania z wczesniejszej iteracji
        std::array<double, N> previousSolutionApproximation = m_currentApproximation;

        do
        {
            iterationNum++;

            //poprzednie przyblizenie = aktualne przeblizenie z poprzedniego etapu
            previousSolutionApproximation = m_currentApproximation;

            calculateNextApproximation(previousSolutionApproximation);
            errorEstimator = calculateErrorEstimator(previousSolutionApproximation);
            residuum = calculateResiduum();

        #ifdef ES_PRINT_STEPS
            std::cout << "Iteracja #" << iterationNum << "\nXn = ";
            MVutil::vector::print(m_currentApproximation);
            std::cout << "\nEstymator bledu: " << errorEstimator << " residuum: " << residuum << "\n";
            std::cout << std::endl;
        #endif // ES_PRINT_STEPS

        } while (iterationNum < this->MAX_ITERATIONS && 
                (residuum > this->TOLERANCE || errorEstimator > this->TOLERANCE));

        return m_currentApproximation;
    }


private:
    /** 
     * rozklad macierzy wspolczynnikow na dwie macierze: L+U oraz D
     * zmienna skladowa m_LUmatrix = L + U
     * zmienna skladowa m_diagonalVector = D
     */
    virtual void decomposeMatrix() override
    {
        for (size_t i = 0; i < N; i++)
        {
            m_diagonalVector[i] = m_LUmatrix[i * N + i];
            m_LUmatrix[i * N + i] = 0.0;
        }
    }

    virtual inline void calculateNextApproximation(const std::array<double, N>& prevApproximation) override
    {
        std::array<double, N> tmpConstantTermsVec;

        //obliczenie wektora z prawej strony rownania Dx = -(L+U)x + b...
        for (size_t i = 0; i < N; i++)
        {
            double sumInRow = 0.0;
            for (size_t j = 0; j < N; j++)
            {
                sumInRow += m_LUmatrix[i * N + j] * prevApproximation[j];
            }
            tmpConstantTermsVec[i] = this->m_constantTermsVector->at(i) - sumInRow;
        }

        //... i pomnozenie go przez macierz odwrotna do macierzy diagonalnej da nowe przyblizenie
        for (size_t i = 0; i < N; i++)
        {
            m_currentApproximation[i] = tmpConstantTermsVec[i] / m_diagonalVector[i];
        }
    }

    virtual double calculateErrorEstimator(const std::array<double, N>& prevApproximation) override
    {
        double max = std::abs(m_currentApproximation[0] - prevApproximation[0]);
        double tmpVal;

        for (size_t i = 1; i < N; i++)
        {
            tmpVal = std::abs(m_currentApproximation[i] - prevApproximation[i]);
            if (tmpVal > max)
                max = tmpVal;
        }

        return max;
    }

    virtual double calculateResiduum() override
    {
        double tmpVal;
        double max = 0.0;
        double sumInRow = 0.0;

        for (size_t i = 0; i < N; i++)
        {
            sumInRow = 0.0;
            for (size_t j = 0; j < N; j++)
            {
                sumInRow += this->m_coefficientsMatrix->at(i * N + j) * m_currentApproximation[j];
            }
            tmpVal = std::abs(sumInRow - this->m_constantTermsVector->at(i));
            if (tmpVal > max)
                max = tmpVal;
        }

        return max;
    }

    //macierz wspolczynnikow z zerami na przekatnej glownej
    std::array<double, N * N> m_LUmatrix;

    // wektor zawierajacy przekatna glowna macierzy wspolczynnikow
    std::array<double, N> m_diagonalVector;

    //wektor rozwiazania ukladu
    std::array<double, N> m_currentApproximation;
};