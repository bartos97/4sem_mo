#include "CrankNicolsonDiscretizer.h"


void CrankNicolsonDiscretizer::CreateMatrix(std::vector<double>& lower_diagonal,
                                            std::vector<double>& diagonal, 
                                            std::vector<double>& upper_diagonal) const
{
    //patrz wyk�ad: s. 138
    //obliczenie Ui,k+1 dla i = 0, 1, ..., n 
    //wymaga rozwi�zania uk�adu liniowych r�wna� algebraicznych;
    //z zadanych warunk�w brzegowych alfa(t) = fi(t) = 1 
    //oraz beta(t) = psi(t) = gamma(t) = theta(t) = 0

    const size_t num_of_nodes_in_space = nodes_space_.size();

    UpdateMatrix(t_start_, lower_diagonal, diagonal, upper_diagonal);

    //�rodkowe wiersze
    for (size_t i = 1; i < num_of_nodes_in_space - 1; ++i)
    {
        upper_diagonal[i] = lambda_param_ / 2.0;
        lower_diagonal[i - 1] = lambda_param_ / 2.0;
        diagonal[i] = -(1.0 + lambda_param_);
    }
}


void CrankNicolsonDiscretizer::UpdateMatrix(const double current_time_value,
                                            std::vector<double>& lower_diagonal,
                                            std::vector<double>& diagonal,
                                            std::vector<double>& upper_diagonal) const
{
    //pierwszy wiersz
    diagonal[0] = -alpha_param_func_(current_time_value) / h_ + beta_param_func_(current_time_value);
    upper_diagonal[0] = alpha_param_func_(current_time_value) / h_;

    //ostatni wiersz:
    //upper jest zrobiona do ko�ca
    *(lower_diagonal.end() - 1) = -phi_param_func_(current_time_value) / h_;
    *(diagonal.end() - 1) = phi_param_func_(current_time_value) / h_ + psi_param_func_(current_time_value);
}


void CrankNicolsonDiscretizer::UpdateFreeTermsVec(std::vector<double>& free_terms,
                                                  const std::vector<double>& result_on_current_time_lvl,
                                                  const double current_time_level) const
{
    const double half_lambda = lambda_param_ / 2.0;
    const double one_m_lambda = 1.0 - lambda_param_;

    //wektor wyraz�w wolnych w tym uk�adzie r�wna� to wektor z wcze�niejszym przbyli�eniem;
    //z pewnymi zmianami:
    free_terms[0] = -gamma_param_func_(current_time_level);
    for (size_t i = 1; i < num_of_nodes_in_space_ - 1; ++i)
    {
        free_terms[i] = -(
            half_lambda * result_on_current_time_lvl[i-1] 
            + one_m_lambda * result_on_current_time_lvl[i]
            + half_lambda * result_on_current_time_lvl[i + 1]
        );
    }
    free_terms[num_of_nodes_in_space_ - 1] = -theta_param_func_(current_time_level);
}
