#pragma once
#include "Discretizer.h"

class CrankNicolsonDiscretizer final : public Discretizer
{
public:
    CrankNicolsonDiscretizer(const double d_factor, const double x_start, const double x_end,
                        const double t_start, const double t_end,
                        double(*const start_condition)(const double t),
                        double(*const alpha_param_func)(const double t),
                        double(*const beta_param_func)(const double t),
                        double(*const gamma_param_func)(const double t),
                        double(*const phi_param_func)(const double t),
                        double(*const psi_param_func)(const double t),
                        double(*const theta_param_func)(const double t))
        : Discretizer(d_factor, x_start, x_end, t_start, t_end, start_condition, 
                      alpha_param_func, beta_param_func, gamma_param_func,
                      phi_param_func, psi_param_func, theta_param_func)
    {}

private:
    void CreateMatrix(std::vector<double>& lower_diagonal,
                      std::vector<double>& diagonal,
                      std::vector<double>& upper_diagonal) const override;
    void UpdateMatrix(const double current_time_value, 
                      std::vector<double>& lower_diagonal,
                      std::vector<double>& diagonal,
                      std::vector<double>& upper_diagonal) const override;
    void UpdateFreeTermsVec(std::vector<double>& free_terms,
                            const std::vector<double>& result_on_current_time_lvl,
                            const double current_time_level) const override;
};
