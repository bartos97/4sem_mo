#pragma once
#include <array>
#include <vector>
#include <iostream>


namespace vecutil {
    template<typename T, std::size_t N>
    T maxNorm(const std::array<T, N>& vector)
    {
        T max = std::abs(vector[0]);
        T tmpVal;

        auto it = vector.begin();
        it++;
        while (it != vector.end())
        {
            tmpVal = std::abs(*it);
            if (tmpVal > max) 
                max = tmpVal;
            it++;
        }

        return max;
    }


    template<typename FloatType>
    FloatType maxNorm(const std::vector<FloatType>& vector)
    {
        FloatType max = std::abs(vector[0]);
        FloatType tmpVal;

        auto it = vector.begin() + 1;
        while (it != vector.end())
        {
            tmpVal = std::abs(*it);
            if (tmpVal > max)
                max = tmpVal;
            ++it;
        }

        return max;
    }


    template<typename FloatType>
    FloatType maxNorm(FloatType* vector, size_t length)
    {
        FloatType max = std::abs(vector[0]);
        FloatType tmpVal;

        for (size_t i = 1; i < length; i++)
        {
            vector++;
            tmpVal = std::abs(*vector);
            if (tmpVal > max)
                max = tmpVal;
        }

        return max;
    }


    template<typename T, std::size_t N>
    void print(const std::array<T, N>& vector)
    {
        std::cout << "[";

        auto it = vector.begin();
        auto end_it = vector.end();
        for (;;)
        {
            std::cout << std::fixed << *it;
            it++;
            if (it != end_it)
                std::cout << ", ";
            else
            {
                std::cout << "]";
                break;
            }
        }        
    }

    template<typename FloatType>
    void print(const std::vector<FloatType>& vector)
    {
        std::cout << "[";

        auto it = vector.begin();
        auto end_it = vector.end();
        for (;;)
        {
            std::cout << std::fixed << *it;
            it++;
            if (it != end_it)
                std::cout << ", ";
            else
            {
                std::cout << "]";
                break;
            }
        }
    }
}

