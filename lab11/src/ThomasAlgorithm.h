#pragma once
#include <vector>

class ThomasAlgorithm
{
public:
    ThomasAlgorithm() = default;

    ThomasAlgorithm(const std::vector<double>& lower_diagonal,
                    const std::vector<double>& diagonal,
                    const std::vector<double>& upper_diagonal,
                    const std::vector<double>& free_terms);

    void AssignAndCalculate(const std::vector<double>& lower_diagonal,
                            const std::vector<double>& diagonal,
                            const std::vector<double>& upper_diagonal,
                            const std::vector<double>& free_terms);

    const std::vector<double>& GetResult() const { return solution_; }

private:
    static void TransformDiagonals(const std::vector<double>& lower_diagonal,
                                   std::vector<double>& diagonal,
                                   const std::vector<double>& upper_diagonal,
                                   std::vector<double>& free_terms);

    void Solve(const std::vector<double>& diagonal,
               const std::vector<double>& upper_diagonal,
               const std::vector<double>& free_terms);

    std::vector<double> solution_;
    std::vector<double> diagonal_copy_;
    std::vector<double> free_terms_copy_;
};
