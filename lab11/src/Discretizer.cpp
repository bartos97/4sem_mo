#include "Discretizer.h"

#include <cmath>
#include <fstream>
#include <locale>
#include <iostream>

#include "ThomasAlgorithm.h"
#include "VectorUtilities.h"


double Discretizer::Calculate(const double h)
{
    static double cached_h;

    //krok siatki czasowej dobierany automatycznie; tak �eby parametr lambda = 1
    lambda_param_ = 1.0; // normalnie to lambda_param = d_factor_ * (dt / (h * h))

    //normalizacja kroku, tak aby r�wnomiernie rozk�ada� ca�y przedzia�
    h_ = (x_end_ - x_start_) / std::floor((x_end_ - x_start_) / h);

    //aby unikn�� oblicze� kiedy podany krok jest taki sam (po normalnizacji) jak ju� obliczony
    if (h_ == cached_h)
        return h_;
    else
        cached_h = h_;

    //normalizacja kroku, tak aby r�wnomiernie rozk�ada� ca�y przedzia�
    double tmp_dt = (lambda_param_ * h_ * h_) / d_factor_;
    dt_ = (t_end_ - t_start_) / std::floor((t_end_ - t_start_) / tmp_dt);

    //parametr lambda wyznaczony dla znormalizowanycj krok�w
    lambda_param_ = d_factor_ * (dt_ / (h_ * h_));

    num_of_nodes_in_space_ = size_t((x_end_ - x_start_) / h_) + 1;
    num_of_nodes_in_time_ = size_t((t_end_ - t_start_) / dt_) + 1;
    full_result_.reserve(num_of_nodes_in_space_ * num_of_nodes_in_time_);

    //tworzenie w�z��w siatki przestrzennej i czasowej
    CreateNet(num_of_nodes_in_space_, num_of_nodes_in_time_);

    //wykonanie warunku pocz�tkowego na zerowym poziomie czasowym
    std::vector<double> result_on_current_time_lvl(num_of_nodes_in_space_);
    ApplyStartCond(result_on_current_time_lvl);
    full_result_.assign(result_on_current_time_lvl.begin(), result_on_current_time_lvl.end());

    //utworzenie macierzy tr�jdiagonalnej z r�wnania
    std::vector<double> lower_diagonal(num_of_nodes_in_space_ - 1);
    std::vector<double> diagonal(num_of_nodes_in_space_);
    std::vector<double> upper_diagonal(num_of_nodes_in_space_ - 1);
    CreateMatrix(lower_diagonal, diagonal, upper_diagonal);

    //wektor wyraz�w wolnych z r�wnania
    std::vector<double> free_terms(num_of_nodes_in_space_);

    double current_time_level = t_start_;
    auto pos_in_result = full_result_.begin();

    ThomasAlgorithm thomas_algorithm;

    for (size_t k = 1; k < num_of_nodes_in_time_; ++k)
    {
        pos_in_result += num_of_nodes_in_space_;
        current_time_level = nodes_time_[k];

        UpdateFreeTermsVec(free_terms, result_on_current_time_lvl, current_time_level);
        UpdateMatrix(current_time_level, lower_diagonal, diagonal, upper_diagonal);

        thomas_algorithm.AssignAndCalculate(lower_diagonal, diagonal, upper_diagonal, free_terms);
        result_on_current_time_lvl = thomas_algorithm.GetResult();

        full_result_.insert(pos_in_result, result_on_current_time_lvl.begin(), result_on_current_time_lvl.end());
    }

    return h_;
}


void Discretizer::SaveToFile(const char* file_name)
{
    std::ofstream file;
    file.exceptions(std::ofstream::badbit);

    try
    {
        file.open(file_name, std::ofstream::out | std::ofstream::trunc);
        file << "t;x;U(x,t)\n"; //nag��wek pliku            

        double current_time_level = 0.0;
        double current_space_node = 0.0;
        for (size_t k = 0; k < num_of_nodes_in_time_; ++k)
        {
            current_time_level = nodes_time_[k];
            for (size_t i = 0; i < num_of_nodes_in_space_; ++i)
            {
                current_space_node = nodes_space_[i];
                file << std::fixed
                    << current_time_level << ';'
                    << current_space_node << ';'
                    << full_result_[k * num_of_nodes_in_space_ + i] << '\n';
            }
        }

        file.close();
    }
    catch (const std::ofstream::failure& e)
    {
        std::cerr << "Error when opening / reading file " << file_name << ":" << e.what() << std::endl;
    }
}


void Discretizer::SaveToFile(const char* file_name,
                             double(*analytic_solution)(const double x, const double t)) const
{
    std::ofstream file;
    file.exceptions(std::ofstream::badbit);

    try
    {
        file.open(file_name, std::ofstream::out | std::ofstream::trunc);
        file << "t;x;U(x,t);analytic\n"; //nag��wek pliku            

        double current_time_level = 0.0;
        double current_space_node = 0.0;
        for (size_t k = 0; k < num_of_nodes_in_time_; ++k)
        {
            current_time_level = nodes_time_[k];
            for (size_t i = 0; i < num_of_nodes_in_space_; ++i)
            {
                current_space_node = nodes_space_[i];
                file << std::fixed
                    << current_time_level << ';'
                    << current_space_node << ';'
                    << full_result_[k * num_of_nodes_in_space_ + i] << ';'
                    << analytic_solution(current_space_node, current_time_level) << '\n';
            }
        }

        file.close();
    }
    catch (const std::ofstream::failure& e)
    {
        std::cerr << "Error when opening / reading file " << file_name << ":" << e.what() << std::endl;
    }
}


void Discretizer::GetResultInLastTimeLevel(std::vector<double>& vector_to_save_in)
{
    GetResultInGivenTimeLevel(num_of_nodes_in_time_ - 1, vector_to_save_in);
}


void Discretizer::GetResultInGivenTimeLevel(const size_t level_num,
                                            std::vector<double>& vector_to_save_in)
{
    vector_to_save_in.resize(num_of_nodes_in_space_);
    for (size_t i = 0; i < num_of_nodes_in_space_; ++i)
    {
        vector_to_save_in[i] = full_result_[level_num * num_of_nodes_in_space_ + i];
    }
}


void Discretizer::CreateNet(const size_t num_of_nodes_in_space,
                            const size_t num_of_nodes_in_time)
{
    //inicjalizacja w�z��w siatki przestrzennej
    nodes_space_.resize(num_of_nodes_in_space);
    double tmp_cum_sum = x_start_;
    for (auto& node : nodes_space_)
    {
        node = tmp_cum_sum;
        tmp_cum_sum += h_;
    }

    //inicjalizacja w�z��w siatki czasowej
    nodes_time_.resize(num_of_nodes_in_time);
    tmp_cum_sum = t_start_;
    for (auto& node : nodes_time_)
    {
        node = tmp_cum_sum;
        tmp_cum_sum += dt_;
    }
}


void Discretizer::ApplyStartCond(std::vector<double>& result)
{
    if (result.size() != nodes_space_.size())
        _ASSERT(false);

    auto it_result = result.begin();
    for (auto& node : nodes_space_)
    {
        *it_result = start_condition_(node);
        it_result++;
        //jest OK bo length(result_on_current_time_lvl) == length(nodes_space)
    }
}
