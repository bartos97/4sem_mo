#pragma once
#include <vector>

class Discretizer
{
protected:
    Discretizer(const double d_factor, const double x_start, const double x_end,
                const double t_start, const double t_end,
                double(*const start_condition)(const double t),
                double(*const alpha_param_func)(const double t),
                double(*const beta_param_func)(const double t),
                double(*const gamma_param_func)(const double t),
                double(*const phi_param_func)(const double t),
                double(*const psi_param_func)(const double t),
                double(*const theta_param_func)(const double t))
        : d_factor_(d_factor),
        x_start_(x_start),
        x_end_(x_end),
        t_start_(t_start),
        t_end_(t_end),
        start_condition_(start_condition),
        alpha_param_func_(alpha_param_func),
        beta_param_func_(beta_param_func),
        gamma_param_func_(gamma_param_func),
        phi_param_func_(phi_param_func),
        psi_param_func_(psi_param_func),
        theta_param_func_(theta_param_func)
    {}

public:
    virtual ~Discretizer() = default;

    virtual double Calculate(const double h);
    virtual void SaveToFile(const char* file_name);
    virtual void SaveToFile(const char* file_name,
                    double(*analytic_solution)(const double x, const double t)) const;

    virtual const std::vector<double>& GetSpaceNodes() const
    {
        return nodes_space_;
    };

    virtual const std::vector<double>& GetTimeNodes() const
    {
        return nodes_time_;
    };

    virtual double GetLastTimeNode() const
    {
        return *(nodes_time_.end() - 1);
    };

    virtual double GetResultAt(const size_t x, const size_t t) const
    {
        return full_result_[t * num_of_nodes_in_space_ + x];
    };

    virtual void GetResultInLastTimeLevel(std::vector<double>& vector_to_save_in);
    virtual void GetResultInGivenTimeLevel(const size_t level_num, std::vector<double>& vector_to_save_in);

public:
    //zadany wsp�czynnik dyfuzji
    const double d_factor_;
    //zadany przedzia� dla zmiennej przestrzennej
    const double x_start_;
    const double x_end_;
    //zadany przedzia� dla zmiennej czasowej
    const double t_start_;
    const double t_end_;

    //warunek pocz�tkowy
    double (*const start_condition_)(const double x);

    //zadane funkcje wynikaj�ce z warunk�w brzegowych
    double (*const alpha_param_func_)(const double t);
    double (*const beta_param_func_)(const double t);
    double (*const gamma_param_func_)(const double t);
    double (*const phi_param_func_)(const double t);
    double (*const psi_param_func_)(const double t);
    double (*const theta_param_func_)(const double t);

protected:
    virtual void CreateNet(const size_t num_of_nodes_in_space, const size_t num_of_nodes_in_time);
    virtual void ApplyStartCond(std::vector<double>& result);

    virtual void CreateMatrix(std::vector<double>& lower_diagonal,
                              std::vector<double>& diagonal,
                              std::vector<double>& upper_diagonal) const = 0;
    virtual void UpdateMatrix(const double current_time_value,
                              std::vector<double>& lower_diagonal,
                              std::vector<double>& diagonal,
                              std::vector<double>& upper_diagonal) const = 0;
    virtual void UpdateFreeTermsVec(std::vector<double>& free_terms,
                                    const std::vector<double>& result_on_current_time_lvl,
                                    const double current_time_level) const = 0;

protected:
    std::vector<double> nodes_space_;
    std::vector<double> nodes_time_;
    std::vector<double> full_result_;

    double h_ = 0.0;
    double dt_ = 0.0;
    double lambda_param_ = 0.0;
    size_t num_of_nodes_in_space_ = 0;
    size_t num_of_nodes_in_time_ = 0;
};
