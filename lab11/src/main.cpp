#define _USE_MATH_DEFINES

#include <cmath>
#include <fstream>
#include <locale>
#include <iostream>
#include <chrono>

#include "VectorUtilities.h"
#include "LaasonenDiscretizer.h"
#include "CrankNicolsonDiscretizer.h"


//zadany wsp�czynnik dyfuzji
const static double kDFactor = 1.0;

//zadany przedzia� dla zmiennej przestrzennej
const static double kXStart = 0.0;
const static double kXEnd = 1.0;

//zadany przedzia� dla zmiennej czasowej
const static double kTStart = 0.0;
const static double kTEnd = 0.5;

//zadany warunek pocz�tkowy
inline double StartingCondition(const double x)
{
    return 1.0 + std::cos(M_PI * x);
}

//zadane funkcje wynikaj�ce z warunk�w brzegowych
inline double AlfaParamFunction(const double t)  { return 1.0; }
inline double BetaParamFunction(const double t)  { return 0.0; }
inline double GammaParamFunction(const double t) { return 0.0; }
inline double PhiParamFunction(const double t)   { return 1.0; }
inline double PsiParamFunction(const double t)   { return 0.0; }
inline double ThetaParamFunction(const double t) { return 0.0; }

//Rozwi�zanie analityczne do test�w
inline double AnalyticSolution(const double x, const double t)
{
    return 1.0 + std::exp(-(M_PI * M_PI) * kDFactor * t) * std::cos(M_PI * x);
}


void ErrorInFunctionOfStepInSpace(Discretizer& discretizer, size_t exec_num, const char* file_path)
{
    const double h_increment = (kXEnd - kXStart) / 5 / exec_num;

    double current_h = 0.0, new_h = 0.0;
    double current_max_error;
    double current_tmax;
    std::vector<double> current_result;

    std::ofstream file;
    file.exceptions(std::ofstream::badbit);

    try
    {
        file.open(file_path, std::ofstream::out | std::ofstream::trunc);
        file << "h;|error|;log10(h);log10(|error|)\n"; //nag��wek pliku    

        for (size_t i = 0; i < exec_num; ++i)
        //for (current_h = 0.005; current_h <= 0.1; current_h += 0.005)
        {
            current_h += h_increment;
            new_h = discretizer.Calculate(current_h);
            current_tmax = discretizer.GetLastTimeNode();

            //wektor b��d�w
            auto nodes = discretizer.GetSpaceNodes();
            discretizer.GetResultInLastTimeLevel(current_result);
            auto it_result = current_result.begin();

            for (auto & x : nodes)
            {
                *it_result = AnalyticSolution(x, current_tmax) - *it_result;
                it_result++;
            }
            current_max_error = vecutil::maxNorm<double>(current_result);

            file << std::fixed
                 << new_h << ';'
                 << current_max_error << ';'
                 << std::log10(current_h) << ';'
                 << std::log10(current_max_error) << '\n';

        }
        
        file.close();
    }
    catch (const std::ofstream::failure& e)
    {
        std::cerr << "Error when opening / reading file " << file_path << ":" << e.what() << std::endl;
    }
}


void ErrorInFunctionOfTime(Discretizer& discretizer, const double h, const char* file_path)
{
    discretizer.Calculate(h);

    std::ofstream file;
    file.exceptions(std::ofstream::badbit);

    try
    {
        file.open(file_path, std::ofstream::out | std::ofstream::trunc);
        file << "t;|error|\n"; //nag��wek pliku   

        std::vector<double> error_vec(discretizer.GetSpaceNodes().size());
        size_t x_index, t_index = 0;

        for (auto && t : discretizer.GetTimeNodes())
        {
            error_vec.clear();

            x_index = 0;
            for (auto && x : discretizer.GetSpaceNodes())
            {
                error_vec.push_back(discretizer.GetResultAt(x_index, t_index) - AnalyticSolution(x, t));
                x_index++;
            }

            file << std::fixed
                 << t << ';'
                 << vecutil::maxNorm<double>(error_vec) << '\n';
            t_index++;
        }

        file.close();
    }
    catch (const std::ofstream::failure& e)
    {
        std::cerr << "Error when opening / reading file " << file_path << ":" << e.what() << std::endl;
    }
}


int main()
{
    //ktore zadania wykonac
    #define ZAD_1 1
    #define ZAD_2 0
    #define ZAD_3 0

    //ilosc krokow w zadaniu 1
    const size_t steps_amount = 100;

    //rozmiar kroku w zadaniach 2 i 3
    const double h = 0.02;

    using namespace std::chrono;
    high_resolution_clock::time_point start_time, end_time;
    long long duration;

    LaasonenDiscretizer laasonen(kDFactor, kXStart, kXEnd, kTStart, kTEnd,
                                 StartingCondition, AlfaParamFunction, BetaParamFunction,
                                 GammaParamFunction, PhiParamFunction, PsiParamFunction,
                                 ThetaParamFunction);
    CrankNicolsonDiscretizer crank(kDFactor, kXStart, kXEnd, kTStart, kTEnd,
                                   StartingCondition, AlfaParamFunction, BetaParamFunction,
                                   GammaParamFunction, PhiParamFunction, PsiParamFunction,
                                   ThetaParamFunction);

    /*************
     * LAASONEN
     **************/
#if ZAD_1
    std::cout << "Laasonen zadanie 1 - blad w funkcji kroku. Zaczynam liczyc..." << std::endl;
    start_time = high_resolution_clock::now();

    ErrorInFunctionOfStepInSpace(laasonen, steps_amount, "out/error_step_space.csv");

    end_time = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(end_time - start_time).count();
    std::cout << "Policzenie tego (z zapisem do pliku) dla " << steps_amount << " wartosci kroku zajelo mi: "
              << duration / 1000.0 << " s\n" << std::endl;
#endif // ZAD_1

#if ZAD_2
    std::cout << "Laasonen zadanie 2 - rozwiazanie. Zaczynam liczyc..." << std::endl;
    start_time = high_resolution_clock::now();

    laasonen.Calculate(h);

    end_time = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(end_time - start_time).count();
    std::cout << "Policzenie tego dla kroku h=" << h << " zajelo mi: "
              << duration / 1000.0 << " s" << std::endl;

    start_time = high_resolution_clock::now();

    laasonen.SaveToFile("out/results_crank.csv", AnalyticSolution);

    end_time = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(end_time - start_time).count();
    std::cout << "a zapis do pliku " << duration / 1000.0 << " s\n" << std::endl;
#endif // ZAD_2

#if ZAD_3
    std::cout << "Laasonen zadanie 3 - blad w funkcji czasu. Zaczynam liczyc..." << std::endl;
    start_time = high_resolution_clock::now();

    ErrorInFunctionOfTime(laasonen, h, "out/error_step_time.csv");

    end_time = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(end_time - start_time).count();
    std::cout << "Policzenie tego (z zapisem do pliku) dla kroku h=" << h << " zajelo mi: "
              << duration / 1000.0 << " s\n" << std::endl;
#endif // ZAD_3



    /************
     * CRANK-NICOLSON
     ***************/
#if ZAD_1
     std::cout << "Crank-Nicolson zadanie 1 - blad w funkcji kroku. Zaczynam liczyc..." << std::endl;
     start_time = high_resolution_clock::now();

     ErrorInFunctionOfStepInSpace(crank, steps_amount, "out/error_step_space_crank.csv");

     end_time = high_resolution_clock::now();
     duration = duration_cast<milliseconds>(end_time - start_time).count();
     std::cout << "Policzenie tego (z zapisem do pliku) dla " << steps_amount << " wartosci kroku zajelo mi: "
               << duration / 1000.0 << " s\n" << std::endl;
 #endif
 
 #if ZAD_2
     std::cout << "Crank-Nicolson zadanie 2 - rozwiazanie. Zaczynam liczyc..." << std::endl;
     start_time = high_resolution_clock::now();

     crank.Calculate(h);

     end_time = high_resolution_clock::now();
     duration = duration_cast<milliseconds>(end_time - start_time).count();
     std::cout << "Policzenie tego dla kroku h=" << h << " zajelo mi: "
               << duration / 1000.0 << " s" << std::endl;

     start_time = high_resolution_clock::now();

     crank.SaveToFile("out/results_crank.csv", AnalyticSolution);

     end_time = high_resolution_clock::now();
     duration = duration_cast<milliseconds>(end_time - start_time).count();
     std::cout << "a zapis do pliku " << duration / 1000.0 << " s\n" << std::endl;
 #endif

 #if ZAD_3
     std::cout << "Crank-Nicolson zadanie 3 - blad w funkcji czasu. Zaczynam liczyc..." << std::endl;
     start_time = high_resolution_clock::now();

     ErrorInFunctionOfTime(crank, h, "out/error_step_time_crank.csv");

     end_time = high_resolution_clock::now();
     duration = duration_cast<milliseconds>(end_time - start_time).count();
     std::cout << "Policzenie tego (z zapisem do pliku) dla kroku h=" << h << " zajelo mi: "
               << duration / 1000.0 << " s\n" << std::endl;
 #endif

    return 0;
}
