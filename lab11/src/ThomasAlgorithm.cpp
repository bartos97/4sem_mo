#include "ThomasAlgorithm.h"
#include <iostream>


ThomasAlgorithm::ThomasAlgorithm(const std::vector<double>& lower_diagonal,
                                 const std::vector<double>& diagonal,
                                 const std::vector<double>& upper_diagonal,
                                 const std::vector<double>& free_terms)
{
    AssignAndCalculate(lower_diagonal, diagonal, upper_diagonal, free_terms);
}


void ThomasAlgorithm::AssignAndCalculate(const std::vector<double>& lower_diagonal,
                                         const std::vector<double>& diagonal,
                                         const std::vector<double>& upper_diagonal,
                                         const std::vector<double>& free_terms)
{
    if (lower_diagonal.size() != upper_diagonal.size() || diagonal.size() != free_terms.size())
    {
        std::cerr << "Nipoprawne wektory do algorytmu Thomasa!" << std::endl;
        return;
    }

    solution_.assign(diagonal.size(), 0.0);
    diagonal_copy_.assign(diagonal.begin(), diagonal.end());
    free_terms_copy_.assign(free_terms.begin(), free_terms.end());

    TransformDiagonals(lower_diagonal, diagonal_copy_, upper_diagonal, free_terms_copy_);
    Solve(diagonal_copy_, upper_diagonal, free_terms_copy_);
}


void ThomasAlgorithm::TransformDiagonals(const std::vector<double>& lower_diagonal, 
                                         std::vector<double>& diagonal, 
                                         const std::vector<double>& upper_diagonal, 
                                         std::vector<double>& free_terms)
{
    for (size_t i = 1; i < diagonal.size(); i++)
    {
        diagonal[i] = diagonal[i] - lower_diagonal[i - 1] * upper_diagonal[i - 1] / diagonal[i - 1];
        free_terms[i] = free_terms[i] - lower_diagonal[i - 1] * free_terms[i - 1] / diagonal[i - 1];
    }
}

void ThomasAlgorithm::Solve(const std::vector<double>& diagonal, 
                            const std::vector<double>& upper_diagonal, 
                            const std::vector<double>& free_terms)
{
    const auto n = diagonal.size();
    solution_[n - 1] = free_terms[n - 1] / diagonal[n - 1];

    for (auto i = n - 1; i > 0; i--)
    {
        solution_[i - 1] = (free_terms[i - 1] - upper_diagonal[i - 1] * solution_[i]) / diagonal[i - 1];
    }
}
