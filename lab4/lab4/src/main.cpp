#include <iostream>
#include <functional>
#include <array>
#include <algorithm>
#include <stdio.h>


#define FUNCTION_TYPE_LAMBDA [](double x, double y, double z) -> double
#define MATRIX_DIM 3
#define MATRIX_ELEM_AMOUNT MATRIX_DIM * MATRIX_DIM
using functionType = std::function<double(double, double, double)>;

template<typename T>
T getMachineEpsilon()
{
    T epsilon, tmpVal;
    tmpVal = epsilon = 1.0;

    while (T(1.0) + (tmpVal /= T(2.0)) > T(1.0))
        epsilon = tmpVal;

    return epsilon;
}


// USTAWIENIA / DANE WEJSCIOWE

// Limit iteracji
static const unsigned int MAX_ITERATIONS = 100;

// Zadana tolerancja wyniku
static const double TOLERANCE = getMachineEpsilon<double>();

// Poczatkowe przyblizenie
static const double startingPoints[3] = { 1.5, 1.5, 1.5 };

// Wektor funkcji ktorych pierwiastki trzeba znalezc
static functionType functionsVector[MATRIX_DIM] = {
    //f1(x) = x^2 + y^2 + z^2 - 2
    FUNCTION_TYPE_LAMBDA { return x * x + y * y + z * z - 2.0; },

    //f2(x) = x^2 + y^2 - 1
    FUNCTION_TYPE_LAMBDA { return x * x + y * y - 1.0; },

    //f3(x) = x^2 - y
    FUNCTION_TYPE_LAMBDA { return x * x - y; }
};

// macierz Jakobiego dla functionsVector
static functionType jacobiMatrix[MATRIX_ELEM_AMOUNT] = {
    FUNCTION_TYPE_LAMBDA { return 2.0 * x; }, //J[1,1]
    FUNCTION_TYPE_LAMBDA { return 2.0 * y; }, //J[1,2]
    FUNCTION_TYPE_LAMBDA { return 2.0 * z; }, //J[1,3]

    FUNCTION_TYPE_LAMBDA { return 2.0 * x; }, //J[2,1]
    FUNCTION_TYPE_LAMBDA { return 2.0 * y; }, //J[2,2]
    FUNCTION_TYPE_LAMBDA { return 0.0; },     //J[2,3]

    FUNCTION_TYPE_LAMBDA { return 2.0 * x; }, //J[3,1]
    FUNCTION_TYPE_LAMBDA { return -1.0; },    //J[3,2]
    FUNCTION_TYPE_LAMBDA { return 0.0; }      //J[3,3]
};

// Wyznacznik jacobiMatrix
static functionType jacobian = FUNCTION_TYPE_LAMBDA { return -4.0*x*z - 8.0*x*y*z; };


/**
 * Funkcja oblicza macierz dopelnien danej macierzy
 * @param resultPtr - wskaznik do miejsca gdzie zapisac wynikowa macierz (tablica rozmiaru MATRIX_ELEM_AMOUNT)
 * @param matrix - referencja do macierzy (JEDNOwymiarowej) na podstawie ktorej bedzie obliczona macierz dopelnien
 */
void cofactorsMatrix(double* resultPtr, const double(&matrix)[MATRIX_ELEM_AMOUNT]);


/**
 * Transponuje podana macierz
 * @param matrix - macierz ktora zostanie transponowana
 */
void transposeMatrix(double(&matrix)[MATRIX_ELEM_AMOUNT]);


/**
 * Mnozy podana macierz przez wektor 
 * @param resultPtr - wskaznik do miejsca gdzie zapisac wynikowy wektor (tablica rozmiaru MATRIX_DIM)
 * @param matrix - macierz o wymiarach MATRIX_DIM x MATRIX_DIM (tablica o rozmiarze MATRIX_ELEM_AMOUNT)
 * @param vector - wektor o rozmiarze MATRIX_DIM
 */
void matrixVectorMultiplication(double* resultPtr, 
                                const double(&matrix)[MATRIX_ELEM_AMOUNT], 
                                const double(&vector)[MATRIX_DIM]);


/**
 * Mnozy podana macierz przez liczbe
 * @param matrix - macierz o wymiarach MATRIX_DIM x MATRIX_DIM (tablica o rozmiarze MATRIX_ELEM_AMOUNT)
 * @param num - liczba przez ktora przemnozyc macierz
 */
void matrixNumberMultiplication(double(&matrix)[MATRIX_ELEM_AMOUNT], double num);


/**
 * Oblicza norme max wektora
 * @param vector - wektor o rozmiarze MATRIX_DIM
 */
double vectorsMaxNorm(const double(&vector)[MATRIX_DIM]);


int main()
{
    unsigned int i = 0, j;

    double residuum = 1.0, errorEstimator = 1.0;
    double errorsVector[MATRIX_DIM];

    double tmpVector[MATRIX_DIM];
    double currentFunVectorValue[MATRIX_DIM];
    double currentJacobiMatrixValue[MATRIX_ELEM_AMOUNT];
    double currentJacobian;
    double currentJacobiInverseMatrix[MATRIX_ELEM_AMOUNT];

    double Xn[MATRIX_DIM]; //poprzednie przyblizenie
    double Xn1[MATRIX_DIM]; //aktualne przyblizenie
    std::copy(startingPoints, startingPoints + MATRIX_DIM, Xn1); //ustaw punkty startowe

    do
    {
        i++;

        //poprzednie przyblizenie = aktualne przeblizenie z poprzedniego etapu
        for (j = 0; j < MATRIX_DIM; j++)
            Xn[j] = Xn1[j];

        //wartosci funkcji w punktach poprzedniego przyblizenia
        for (j = 0; j < MATRIX_DIM; j++)
            currentFunVectorValue[j] = functionsVector[j](Xn[0], Xn[1], Xn[2]);

        //wartosci macierzy jakobiego w punktach poprzedniego przyblizenia
        for (j = 0; j < MATRIX_ELEM_AMOUNT; j++)
            currentJacobiMatrixValue[j] = jacobiMatrix[j](Xn[0], Xn[1], Xn[2]);

        //jakobian w punktach poprzedniego przyblizenia
        currentJacobian = jacobian(Xn[0], Xn[1], Xn[2]);

        //macierz dopelnien algebraicznych aktualnej macierzy jakobiego
        cofactorsMatrix(currentJacobiInverseMatrix, currentJacobiMatrixValue);

        //transponuj macierz dopelnien
        transposeMatrix(currentJacobiInverseMatrix);
        
        //wreszcie mamy odwrotna macierz jakobiego
        matrixNumberMultiplication(currentJacobiInverseMatrix, 1 / currentJacobian);
        
        // pomnoz odwrotna macierz jakobiego przez wektor funkcji
        matrixVectorMultiplication(tmpVector, currentJacobiInverseMatrix, currentFunVectorValue);

        //obliczenie nowego przyblizenia
        for (j = 0; j < MATRIX_DIM; j++)
            Xn1[j] = Xn[j] - tmpVector[j];

        //obliczenie wektora estymatorow bledow
        for (j = 0; j < MATRIX_DIM; j++)
            errorsVector[j] = Xn1[j] - Xn[j];

        errorEstimator = vectorsMaxNorm(errorsVector);
        residuum = vectorsMaxNorm(currentFunVectorValue);

        printf("i = %-3d Xn1[0] = %-27.20lf Xn1[1] = %-27.20lf Xn1[2] = %-27.20lf En = %-27.20lf |f(Xn)| = %-27.20lf \n", 
               i, Xn1[0], Xn1[1], Xn1[2], errorEstimator, residuum);

    } while (i < MAX_ITERATIONS && residuum > TOLERANCE && errorEstimator > TOLERANCE);
    
    return 0;
}


void cofactorsMatrix(double* resultPtr, const double(&matrix)[MATRIX_ELEM_AMOUNT])
{
    *resultPtr     =   matrix[1 *MATRIX_DIM+ 1] * matrix[2 *MATRIX_DIM+ 2] - matrix[1 *MATRIX_DIM+ 2] * matrix[2 *MATRIX_DIM+ 1];
    *(++resultPtr) = -(matrix[1 *MATRIX_DIM+ 0] * matrix[2 *MATRIX_DIM+ 2] - matrix[1 *MATRIX_DIM+ 2] * matrix[2 *MATRIX_DIM+ 0]);
    *(++resultPtr) =   matrix[1 *MATRIX_DIM+ 0] * matrix[2 *MATRIX_DIM+ 1] - matrix[1 *MATRIX_DIM+ 1] * matrix[2 *MATRIX_DIM+ 0];
    *(++resultPtr) = -(matrix[0 *MATRIX_DIM+ 1] * matrix[2 *MATRIX_DIM+ 2] - matrix[2 *MATRIX_DIM+ 1] * matrix[0 *MATRIX_DIM+ 2]);
    *(++resultPtr) =   matrix[0 *MATRIX_DIM+ 0] * matrix[2 *MATRIX_DIM+ 2] - matrix[2 *MATRIX_DIM+ 0] * matrix[0 *MATRIX_DIM+ 2];
    *(++resultPtr) = -(matrix[0 *MATRIX_DIM+ 0] * matrix[2 *MATRIX_DIM+ 1] - matrix[2 *MATRIX_DIM+ 0] * matrix[0 *MATRIX_DIM+ 1]);
    *(++resultPtr) =   matrix[0 *MATRIX_DIM+ 1] * matrix[1 *MATRIX_DIM+ 2] - matrix[1 *MATRIX_DIM+ 1] * matrix[0 *MATRIX_DIM+ 2];
    *(++resultPtr) = -(matrix[0 *MATRIX_DIM+ 0] * matrix[1 *MATRIX_DIM+ 2] - matrix[1 *MATRIX_DIM+ 0] * matrix[0 *MATRIX_DIM+ 2]);
    *(++resultPtr) =   matrix[0 *MATRIX_DIM+ 0] * matrix[1 *MATRIX_DIM+ 1] - matrix[1 *MATRIX_DIM+ 0] * matrix[0 *MATRIX_DIM+ 1];

}


void transposeMatrix(double(&matrix)[MATRIX_ELEM_AMOUNT])
{
    // tymczasowa kopia macierzy do transpozycji, tworzona na stosie
    double buffer[MATRIX_ELEM_AMOUNT];
    std::copy(matrix, matrix + MATRIX_ELEM_AMOUNT, buffer);

    buffer[0 * MATRIX_DIM + 0] = matrix[0 * MATRIX_DIM + 0];
    buffer[0 * MATRIX_DIM + 1] = matrix[1 * MATRIX_DIM + 0];
    buffer[0 * MATRIX_DIM + 2] = matrix[2 * MATRIX_DIM + 0];
    buffer[1 * MATRIX_DIM + 0] = matrix[0 * MATRIX_DIM + 1];
    buffer[1 * MATRIX_DIM + 1] = matrix[1 * MATRIX_DIM + 1];
    buffer[1 * MATRIX_DIM + 2] = matrix[2 * MATRIX_DIM + 1];
    buffer[2 * MATRIX_DIM + 0] = matrix[0 * MATRIX_DIM + 2];
    buffer[2 * MATRIX_DIM + 1] = matrix[1 * MATRIX_DIM + 2];
    buffer[2 * MATRIX_DIM + 2] = matrix[2 * MATRIX_DIM + 2];

    //kopiuj z powrotem do matrix
    std::copy(buffer, buffer + MATRIX_ELEM_AMOUNT, matrix);
}


void matrixVectorMultiplication(double* resultPtr,
                                const double(&matrix)[MATRIX_ELEM_AMOUNT],
                                const double(&vector)[MATRIX_DIM])
{
    for (size_t i = 0; i < MATRIX_DIM; i++)
    {
        *(resultPtr + i) = matrix[i *MATRIX_DIM+ 0] * vector[0] + 
                           matrix[i *MATRIX_DIM+ 1] * vector[1] + 
                           matrix[i *MATRIX_DIM+ 2] * vector[2];
    }
}


void matrixNumberMultiplication(double(&matrix)[MATRIX_ELEM_AMOUNT], double num)
{
    for (size_t i = 0; i < MATRIX_ELEM_AMOUNT; i++)
    {
        matrix[i] *= num;
    }
}


double vectorsMaxNorm(const double(&vector)[MATRIX_DIM])
{
    double max = std::abs(vector[0]);
    for (size_t i = 1; i < MATRIX_DIM; i++)
        if (std::abs(vector[i]) > max) max = std::abs(vector[i]);
    return max;
}