#pragma once
#include <array>
#include <iostream>
#include <iomanip>

namespace MVutil {
    
    namespace vector {
        template<typename T, std::size_t N>
        T maxNorm(const std::array<T, N>& vector)
        {
            T max = std::abs(vector[0]);
            T tmpVal;

            auto it = vector.begin();
            it++;
            while (it != vector.end())
            {
                tmpVal = std::abs(*it);
                if (tmpVal > max) 
                    max = tmpVal;
                it++;
            }

            return max;
        }


        template<typename FloatType>
        FloatType maxNorm(const std::vector<FloatType>& vector)
        {
            FloatType max = std::abs(vector[0]);
            FloatType tmpVal;

            auto it = vector.begin() + 1;
            while (it != vector.end())
            {
                tmpVal = std::abs(*it);
                if (tmpVal > max)
                    max = tmpVal;
                it++;
            }

            return max;
        }


        template<typename T, std::size_t N>
        void print(const std::array<T, N>& vector)
        {
            std::cout << "[";

            auto it = vector.begin();
            while (it != vector.end())
            {
                //std::cout << std::setw(15) << std::fixed << std::setprecision(10) << *it;
                std::cout << *it;
                if (it != vector.end() - 1)
                    std::cout << ", ";
                it++;
            }

            std::cout << "]";
        }
    }
}
