#include <clocale>

#include <iostream>
#include <iomanip>
#include <fstream>

#include <cmath>

#include <array>
#include <vector>

#include "MatrixVectorUtilities.h"
#include "ThomasAlgorithm.h"


//Zadane funkcje z rownania rozniczkowego
static inline double p() { return 1.0; }
static inline double q() { return 0.0; }
static inline double r() { return 4.0; }
static inline double s(const double& x) { return std::tan(x); }

//Warunki brzegowe
static const double ALFA = 0.0;
static const double BETA = 1.0;
static const double GAMMA = 0.0;
static const double FI = 0.0;
static const double PSI = 1.0;
static const double TETA = -0.5;

//Obszar rozwiazania
static const double X_START = 0.0;
static const double X_END = M_PI_4;

static const size_t NODES_AMOUNT = 100;

static std::array<double, NODES_AMOUNT> glob_numerovResult;
static std::array<double, NODES_AMOUNT> glob_conventionalResult;
static std::array<double, NODES_AMOUNT> glob_analyticResult;
static std::array<double, NODES_AMOUNT> glob_arguments;


//Rozwiazanie analityczne, z tresci zadania
static inline double analyticSolution(const double& x)
{
    double result;
    double cosine = std::cos(x);
    double cosine2 = std::cos(2.0 * x);
    double sine2 = std::sin(2.0 * x);
    result = 2.0 * x * cosine2 + 2.0 * sine2 - std::log(2.0) * sine2 - 2.0 * std::log(cosine) * sine2;
    return result / 4.0;
}


//Zapisuje wyniki obliczen (zmienne globalne) do pliku "results.csv"
static void saveResults()
{
    std::string fileName = "results.csv";
    std::ofstream file;
    file.exceptions(std::ofstream::badbit);

    try
    {
        file.open(fileName, std::ofstream::trunc);

        auto it_numerov = glob_numerovResult.begin();
        auto it_conventional = glob_conventionalResult.begin();
        auto it_analytic = glob_analyticResult.begin();
        auto it_arguments = glob_arguments.begin();

        file << "x;"
             << "U(x);"
             << "conv(x);"
             << "num(x);\n";

        // ten sam rozmiar - nie ma sensu sprawdzac wszystkich iteratorow
        while (it_numerov != glob_numerovResult.end())
        {
            file << std::fixed
                 << *it_arguments << ";"
                 << *it_analytic << ";"
                 << *it_conventional << ";"
                 << *it_numerov << "\n";
            it_numerov++;
            it_conventional++;
            it_analytic++;
            it_arguments++;
        }
    }
    catch (const std::ofstream::failure& e)
    {
        std::cerr << "Error when opening / reading file " << fileName << ":" << e.what() << std::endl;
    }

    file.close();
}


//Tworzy n wezlow siatki jednorodnej o kroku "step" w podanym wektorze;
//siatka robiona od danego punktu startowego
static void createNet(std::vector<double>& vec, double startPoint, double step, const size_t n)
{
    vec.assign(n, 0.0);
    auto it = vec.begin();
    *it = startPoint;
    it++;

    while (it != vec.end())
    {
        *it = *(it - 1) + step;
        it++;
    }
}


//Ustawia glob_arguments i glob_analyticResult wed�ug podanej siatki jednorodnej
static void setupArgsAnalytic(std::vector<double>& net)
{
    static bool isSet = false;
    if (isSet)
        return;

    if (net.capacity() != NODES_AMOUNT)
    {
        std::cerr << "setupArgsAnalytic: Podany wektor musi miec zarezerwowane miejsce w ilosci NODES_AMOUNT" << std::endl;
        return;
    }

    auto it_net = net.begin();
    auto it_args = glob_arguments.begin();
    auto it_analytic = glob_analyticResult.begin();

    while (it_net != net.end())
    {
        *it_args = *it_net;
        *it_analytic = analyticSolution(*it_net);
        it_net++;
        it_args++;
        it_analytic++;
    }

    isSet = true;
}


void numerovCalcVectors(std::vector<double>& net,
                        std::vector<double>& lowerDiagonal,
                        std::vector<double>& diagonal,
                        std::vector<double>& upperDiagonal,
                        std::vector<double>& freeTerms,
                        double step,
                        size_t nodesAmount)
{
    upperDiagonal[0] = ALFA / step;
    diagonal[0] = BETA - ALFA / step;
    freeTerms[0] = -GAMMA;

    for (size_t i = 1; i < nodesAmount - 1; i++)
    {
        lowerDiagonal[i - 1] = p() / (step * step) + r() / 12.0;
        diagonal[i] = (-2.0 * p()) / (step * step) + r() * 10.0 / 12.0;
        upperDiagonal[i] = p() / (step * step) + r() / 12.0;
        freeTerms[i] = -(s(net[i - 1]) + 10.0 * s(net[i]) + s(net[i + 1])) / 12.0;
    }

    lowerDiagonal[nodesAmount - 2] = -FI / step;
    diagonal[nodesAmount - 1] = -FI / step + PSI;
    freeTerms[nodesAmount - 1] = -TETA;
}


void conventionalCalcVectors(std::vector<double> & net,
                             std::vector<double> & lowerDiagonal,
                             std::vector<double> & diagonal,
                             std::vector<double> & upperDiagonal,
                             std::vector<double> & freeTerms,
                             double step,
                             size_t nodesAmount)
{
    upperDiagonal[0] = ALFA / step;
    diagonal[0] = BETA - ALFA / step;
    freeTerms[0] = -GAMMA;

    for (size_t i = 1; i < nodesAmount - 1; i++)
    {
        lowerDiagonal[i - 1] = p() / (step * step) - q() / (2.0 * step);
        diagonal[i] = (-2.0 * p()) / (step * step) + r();
        upperDiagonal[i] = p() / (step * step) - q() / (2.0 * step);
        freeTerms[i] = -s(net[i]);
    }

    lowerDiagonal[nodesAmount - 2] = -FI / step;
    diagonal[nodesAmount - 1] = -FI / step + PSI;
    freeTerms[nodesAmount - 1] = -TETA;
}


void numerovSaveResult(const std::vector<double> & result)
{
    auto it_numerov = glob_numerovResult.begin();
    for (double val : result)
    {
        *it_numerov = val;
        it_numerov++;
    }
}


void conventionalSaveResult(const std::vector<double> & result)
{
    auto it_conventional = glob_conventionalResult.begin();
    for (double val : result)
    {
        *it_conventional = val;
        it_conventional++;
    }
}


void calcError(std::vector<double> & error,
               const std::vector<double> & result,
               const std::vector<double> & analyticSolution,
               size_t nodesAmount)
{
    for (size_t i = 1; i < nodesAmount - 1; i++)
    {
        error[i] = std::abs(result[i] - analyticSolution[i]);
    }
}


//Dyskretyzacja numerova i konwencjonalna trzypunktowa na podanej siatce jednorodnej;
//zwraca pare max bladow bezwzglednych, odpowiednio numerova i konwecjonalna
//jesli ilosc wezlow w podanej siatce zgadza sie z NODES_AMOUNT 
//to zapisuje wyniki do odpowiednich zmiennych globalnych
static std::pair<double, double> discretization(std::vector<double>& net, double step)
{
    const size_t nodesAmount = net.size();

    std::vector<double> lowerDiagonal(nodesAmount, 0.0);
    std::vector<double> diagonal(nodesAmount, 0.0);
    std::vector<double> upperDiagonal(nodesAmount, 0.0);
    std::vector<double> freeTerms(nodesAmount, 0.0);

    std::vector<double> error(nodesAmount, 0.0);
    std::vector<double> analyticSolutionOnNet(nodesAmount, 0.0);

    auto it_solutionNet = analyticSolutionOnNet.begin();
    auto it_net = net.begin();
    while (it_solutionNet != analyticSolutionOnNet.end())
    {
        *it_solutionNet = analyticSolution(*it_net);
        it_solutionNet++;
        it_net++;
    }

    //Obliczenia dla metody numerova
    numerovCalcVectors(net, lowerDiagonal, diagonal, upperDiagonal, freeTerms, step, nodesAmount);

    ThomasAlgorithm setSolver(lowerDiagonal, diagonal, upperDiagonal, freeTerms);
    auto result = setSolver.getResult();

    if (NODES_AMOUNT == nodesAmount)
    {
        setupArgsAnalytic(net);
        numerovSaveResult(result);
    }

    calcError(error, result, analyticSolutionOnNet, nodesAmount);
    double numerovError = MVutil::vector::maxNorm<double>(error);

    //Obliczenia dla metody konwencjonalnej

    //zeruj kontenery
    lowerDiagonal.assign(nodesAmount, 0.0);
    diagonal.assign(nodesAmount, 0.0);
    upperDiagonal.assign(nodesAmount, 0.0);
    freeTerms.assign(nodesAmount, 0.0);
    error.assign(nodesAmount, 0.0);

    conventionalCalcVectors(net, lowerDiagonal, diagonal, upperDiagonal, freeTerms, step, nodesAmount);

    setSolver.assignData(lowerDiagonal, diagonal, upperDiagonal, freeTerms);
    result = setSolver.getResult();

    if (NODES_AMOUNT == nodesAmount)
    {
        conventionalSaveResult(result);
    }

    calcError(error, result, analyticSolutionOnNet, nodesAmount);
    double conventionalError = MVutil::vector::maxNorm<double>(error);

    return std::make_pair(numerovError, conventionalError);
}


int main()
{
    //zeby w liczbach dzisietnych miec przecinki
    std::locale::global(std::locale("pl_PL.UTF8"));

    std::string fileName = "errorsData.csv";
    std::ofstream file;
    file.exceptions(std::ofstream::badbit);

    try
    {
        file.open(fileName, std::ofstream::trunc);
        file << "lg(h);"
             << "lg(num);"
             << "lg(conv);\n";

        double step = 0.1;
        std::vector<double> net;
        size_t currentNodesAmount = NODES_AMOUNT;

        while (step > 9e-6)
        {
            step = (X_END - X_START) / (currentNodesAmount - 1);
            createNet(net, X_START, step, currentNodesAmount);
            auto errors = discretization(net, step);

            file << std::fixed
                 << std::log10(step) << ";"
                 << std::log10(errors.first) << ";" //numerow
                 << std::log10(errors.second) << ";" //conventionl
                 << "\n";

            //if (step > 9e-4)
            //{
            //    currentNodesAmount += 50;
            //}
            //else
            //{
            //    currentNodesAmount += 500;
            //}
            currentNodesAmount += NODES_AMOUNT;
        }      
    }
    catch (const std::ofstream::failure & e)
    {
        std::cerr << "Error when opening / reading file " << fileName << ":" << e.what() << std::endl;
    }

    file.close();
    saveResults();    

    return 0;
}
