#pragma once
#include <vector>

class ThomasAlgorithm
{
public:
    ThomasAlgorithm(std::vector<double>& lowerDiagonal, 
                    std::vector<double>& diagonal,
                    std::vector<double>& upperDiagonal,
                    std::vector<double>& freeTerms);

    void assignData(std::vector<double>& lowerDiagonal,
                    std::vector<double>& diagonal,
                    std::vector<double>& upperDiagonal,
                    std::vector<double>& freeTerms);

    const std::vector<double>& getResult() {
        return m_solution;
    }

private:
    void transformDiagonals(std::vector<double>& lowerDiagonal,
                            std::vector<double>& diagonal,
                            std::vector<double>& upperDiagonal,
                            std::vector<double>& freeTerms);

    void solve(std::vector<double>& diagonal,
                                     std::vector<double>& upperDiagonal,
                                     std::vector<double>& freeTerms);

    std::vector<double> m_solution;
};

