#include "ThomasAlgorithm.h"
#include <iostream>


ThomasAlgorithm::ThomasAlgorithm(std::vector<double>& lowerDiagonal,
                                 std::vector<double>& diagonal,
                                 std::vector<double>& upperDiagonal,
                                 std::vector<double>& freeTerms)
{
    assignData(lowerDiagonal, diagonal, upperDiagonal, freeTerms);
}


void ThomasAlgorithm::assignData(std::vector<double>& lowerDiagonal,
                                 std::vector<double>& diagonal,
                                 std::vector<double>& upperDiagonal,
                                 std::vector<double>& freeTerms)
{
    if (lowerDiagonal.size() != upperDiagonal.size() || diagonal.size() != freeTerms.size())
    {
        std::cerr << "Nipoprawne wektory do algorytmu Thomasa!" << std::endl;
        return;
    }

    m_solution.assign(diagonal.size(), 0.0);

    transformDiagonals(lowerDiagonal, diagonal, upperDiagonal, freeTerms);
    solve(diagonal, upperDiagonal, freeTerms);
}


void ThomasAlgorithm::transformDiagonals(std::vector<double>& lowerDiagonal, std::vector<double>& diagonal, std::vector<double>& upperDiagonal, std::vector<double>& freeTerms)
{
    auto it_lower = lowerDiagonal.begin();
    auto it_upper = upperDiagonal.begin();
    auto it_diag = diagonal.begin();
    auto it_diagNext = it_diag + 1;
    auto it_freeTerm = freeTerms.begin();
    auto it_freeTermNext = it_freeTerm + 1;

    while (it_diagNext != diagonal.end())
    {
        *it_diagNext = *it_diagNext - *it_lower * *it_upper / *it_diag;
        *it_freeTermNext = *it_freeTermNext - *it_lower * *it_freeTerm / *it_diag;

        it_lower++;
        it_upper++;
        it_diag++;
        it_diagNext++;
        it_freeTerm++;
        it_freeTermNext++;
    }

    //for (size_t i = 1; i < diagonal.size(); i++)
    //{
    //    diagonal[i] = diagonal[i] - lowerDiagonal[i - 1] * upperDiagonal[i - 1] / diagonal[i - 1];
    //    freeTerms[i] = freeTerms[i] - lowerDiagonal[i - 1] * freeTerms[i - 1] / diagonal[i - 1];
    //}
}


void ThomasAlgorithm::solve(std::vector<double>& diagonal, std::vector<double>& upperDiagonal, std::vector<double>& freeTerms)
{
    size_t n = diagonal.size();

    m_solution[n - 1] = freeTerms[n - 1] / diagonal[n - 1];

    for (int i = n - 2; i >= 0; i--)
    {
        m_solution[i] = (freeTerms[i] - upperDiagonal[i] * m_solution[i + 1]) / diagonal[i];
    }
}
