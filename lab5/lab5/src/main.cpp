﻿#include <iostream>
#include <iomanip>
#include <cmath>


/**
 * Wypisuje na standardowe wyjscie podana macierz w sformatowany sposob
 * @param matrix - macierz do wyświetlenia
 */
void printMatrix(double matrix[4][4]);


/**
 * Wypisuje na standardowe wyjscie podany wektor w sformatowany sposob
 * @param vector Wektor do wyświetlenia
 */
void printVector(double vector[4]);


/**
 * Redukcja Gaussa na wierszu zawierajacym podany element
 * @param matrix - macierz na ktorej beda wykonane dzialania
 * @param ip - ilosc wierszy danej podmacierzy
 * @param jp - ilosc kolumn danej podmacierzy
 */
void gaussRowEliminate(double matrix[4][4], int ip, int jp);


/**
 * Zamienia miejscami wiersze w podanej macierzy, wg algorytmu dla eliminacji Gaussa
 * @param matrix - marcierz w której zostaną zamienione wiersze
 * @param i - ilosc wierszy w macierzy
 * @param j - ilosc kolumn w macierzy
 * @param vector - wektor, w ktorym analogicznie zostane zamienione skladniki
 */
void matrixSwapRows(double matrix[4][4], int i, int j, double vector[4]);


/**
 * Oblicza wektor rozwiązań macierzy L
 * @param matrix Macierz do obliczeń
 * @param y Wektor rozwiązania
 */
void calcVectorForL(double matrix[4][4], double y[4], double vector[4]);


/**
 * Oblicza wektor rozwiązań macierzy U
 * @param matrix Macierz do obliczeń
 * @param x Wektor rozwiązania
 */
void calcVectorForU(double matrix[4][4], double x[4], double vector[4]);


/**
 * Wyswietla tylko macierz trojkatna dolna z podanej macierzy
 * @param matrix Macierz wejściowa
 */
void printMatrixL(double matrix[4][4]);


/**
 * Wyswietla tylko macierz trojkatna gorna z podanej macierzy
 * @param matrix Macierz wejściowa
 */
void printMatrixU(double matrix[4][4]);


int main()
{
    double matrix[4][4] =
    {
        {1.0,   20.0, -30.0, -4.0, },
        {4.0,   20.0, -6.0,   50.0 },
        {9.0,  -18.0,  12.0, -11.0 },
        {16.0, -15.0,  14.0,  130.0}
    };
    double vector[4] = {0, 114, -5, 177};

    double y[4];
    double x[4];

    std::cout << "Dane wejsciowa" << std::endl;
    printMatrix(matrix);
    printVector(vector);

    for (int i = 0; i < 4; i++)
    {
        if (matrix[i][i] == 0)
        {
            std::cout << "Wybor czesciowy dla:" << "[" << i << ", " << i << "]" << std::endl;
            matrixSwapRows(matrix, i, i, vector);
            printMatrix(matrix);
        }

        std::cout << "Macierz po eliminacji Gaussa od " << "[" << i << ", " << i << "]" << std::endl;

        gaussRowEliminate(matrix, i, i);
        printMatrix(matrix);
    }

    std::cout << "Macierz L:" << std::endl;
    printMatrixL(matrix);

    std::cout << "Macierz U:" << std::endl;
    printMatrixU(matrix);

    std::cout << "Wektor y: ";
    calcVectorForL(matrix, y, vector);
    printVector(y);

    std::cout << "Wektor x: ";
    calcVectorForU(matrix, x, y);
    printVector(x);

    return 0;
}


void printMatrix(double matrix[4][4])
{
    for (int i = 0; i < 4; i++)
    {
        std::cout << "| ";
        for (int j = 0; j < 4; j++)
        {
            std::cout.precision(5);
            std::cout << std::left << std::setw(8) << matrix[i][j];
        }
        std::cout << "| \n";
    }
    std::cout << std::endl;
}


void printVector(double vector[4])
{
    std::cout << "[";
    for (int i = 0; i < 4; i++)
    {
        std::cout.precision(3);
        std::cout << vector[i] << " ";
    }

    std::cout << "]\n" << std::endl;
}


void gaussRowEliminate(double matrix[4][4], int ip, int jp)
{
    for (int i = ip + 1; i < 4; i++)
    {
        matrix[i][jp] = matrix[i][jp] / matrix[ip][jp];
        for (int j = jp + 1; j < 4; j++)
            matrix[i][j] = matrix[i][j] - matrix[i][jp] * matrix[ip][j];
    }

}


void matrixSwapRows(double matrix[4][4], int i, int j, double vector[4])
{
    int max = i;
    double tmp;

    if (max != 3)
    {
        for (int d = i + 1; d < 4; d++)
        {
            if (std::abs(matrix[d][j]) > std::abs(matrix[max][j]))
            {
                max = d;
            }
        }
    }

    for (int i = 0; i < 4; i++)
    {
        tmp = matrix[max][i];
        matrix[max][i] = matrix[j][i];
        matrix[j][i] = tmp;
    }

    tmp = vector[max];
    vector[max] = vector[j];
    vector[j] = tmp;
}


void calcVectorForL(double matrix[4][4], double y[4], double vector[4])
{
    for (int i = 0; i < 4; i++)
    {
        double result = 0.0;
        for (int j = 0; j <= i - 1; j++)
        {
            result = result + matrix[i][j] * y[j];
        }
        y[i] = (vector[i] - result) / 1.0;
    }
}


void calcVectorForU(double matrix[4][4], double x[4], double vector[4])
{
    for (int i = 3; i >= 0; i--)
    {
        double result = 0.0;
        for (int j = i + 1; j < 4; j++)
        {
            result = result + matrix[i][j] * x[j];
        }
        x[i] = (vector[i] - result) / matrix[i][i];
    }
}


void printMatrixL(double matrix[4][4])
{
    int i = 0, j = 0;
    double matrix_l[4][4];

    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            if (i == j)
                matrix_l[i][j] = 1.0;
            else
                matrix_l[i][j] = 0.0;
        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j <= i - 1; j++)
        {
            matrix_l[i][j] = matrix[i][j];
        }
    }

    for (i = 0; i < 4; i++)
    {
        std::cout << "| ";
        for (j = 0; j < 4; j++)
        {
            std::cout.precision(5);
            std::cout << std::setw(8) << matrix_l[i][j];
        }
        std::cout << " |\n";

    }
    std::cout << std::endl;
}


void printMatrixU(double matrix[4][4])
{
    double matrix_u[4][4];

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            matrix_u[i][j] = 0.0;

        }
    }

    for (int i = 3; i >= 0; i--)
    {
        for (int j = i; j < 4; j++)
        {
            matrix_u[i][j] = matrix[i][j];
        }
    }

    for (int i = 0; i < 4; i++)
    {
        std::cout << "| ";
        for (int j = 0; j < 4; j++)
        {
            std::cout.precision(5);
            std::cout << std::setw(8) << matrix_u[i][j];
        }
        std::cout << " |\n";

    }
    std::cout << std::endl;
}
