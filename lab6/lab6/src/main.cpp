#include <iostream>
#include <stdlib.h>

// procedura ktora operuje na macierzy A
void redukcjaMacierzA(double D[], double U[], double L[], double K[], int N);

// procedura ktora operuje na wektorze b
void redukcjaWektorB(double L[], double K[], double B[], double R[], int N);

void rozwiazanie(double X[], double K[], double R[], double U[], int N);
void wyswietlWynik(double X[], int N);

int main()
{
    const unsigned int N = 6;

    double D[N];    
    double U[N - 1];
    double L[N - 1];
    double X[N];    
    double B[N];    

    // Dane z zadania
    D[0] = 30;
    D[1] = 20;
    D[2] = 10;
    D[3] = 10;
    D[4] = 20;
    D[5] = 30;

    U[0] = 2.0 / 3.0;
    U[1] = 5.0 / 6.0;
    U[2] = 9.0 / 10.0;
    U[3] = 13.0 / 14.0;
    U[4] = 17.0 / 18.0;

    L[0] = 3.0 / 4.0;
    L[1] = 7.0 / 8.0;
    L[2] = 11.0 / 12.0;
    L[3] = 15.0 / 16.0;
    L[4] = 19.0 / 20.0;

    B[0] = 94.0 / 3.0;
    B[1] = 173.0 / 4.0;
    B[2] = 581.0 / 20.0;
    B[3] = -815.0 / 28.0;
    B[4] = -6301.0 / 144.0;
    B[5] = -319.0 / 10.0;

    // K to wektor eta (z wykladu)
    double K[N];
    redukcjaMacierzA(D, U, L, K, N);

    //Tworzymy tablice R ktora zastapi w redukcji B w macierzy B:
    double R[N];
    redukcjaWektorB(L, K, B, R, N);

    rozwiazanie(X, K, R, U, N);
    wyswietlWynik(X, N);

    return 0;
}

void redukcjaMacierzA(double D[], double U[], double L[], double K[], int N)
{
    K[0] = D[0];

    for (int i = 1; i < N; i++)
        K[i] = D[i] - L[i - 1] * U[i - 1] / K[i - 1];
}

void redukcjaWektorB(double L[], double K[], double B[], double R[], int N)
{
    R[0] = B[0];

    for (int i = 1; i < N; i++)
        R[i] = B[i] - L[i - 1] * R[i - 1] / K[i - 1];
}

void rozwiazanie(double X[], double K[], double R[], double U[], int N)
{
    X[N - 1] = R[N - 1] / K[N - 1];

    for (int i = N - 2; i >= 0; i--)
        X[i] = (R[i] - U[i] * X[i + 1]) / K[i];
}

void wyswietlWynik(double X[], int N)
{
    std::cout << "Wynik:\n\n";
    for (int i = 0; i < N; i++)
    {
        std::cout << "X" << i + 1 << " = ";
        std::cout << X[i] << std::endl;
    }
}