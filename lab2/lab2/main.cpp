#include <math.h>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <fstream>
#include <limits>

#define MAX_TAYLOR_ITERS 20


inline double f(double x) {
	return (1.0 - exp(-x)) / x;
}


double taylorSeries(double x) {
	if (x <= 0.0) return 0.0;

	double wholeSum = 1.0;
	double singleSumElem = 1.0;
	double singleSumElemTop = 1.0;
	double singleSumElemBottom = 1.0;

	for (size_t i = 2; i < MAX_TAYLOR_ITERS; i++) {
		singleSumElemTop *= x;
		singleSumElemBottom *= i;
		singleSumElem = singleSumElemTop / singleSumElemBottom;

		//nieparzyste wyrazy szeregu
		if (i % 2)
			wholeSum += singleSumElem;
		else
			wholeSum -= singleSumElem;
	}

	return wholeSum;
}


int main() {
	//std::cout << taylorSeries(1);
	std::ifstream inputFile("./dane.txt", std::ios::in);
	std::ofstream outputFile("./out.txt", std::ios::out);

	if (!inputFile.is_open() || !outputFile.is_open()) {
		std::cerr << "Blad otwierania plikow" << std::endl;
		return 0;
	}

	//pomocnicze zmienne
	const auto formatWidth		= std::numeric_limits<double>::digits10 + 6;
	const auto formatPrecision	= std::numeric_limits<double>::digits10 + 1;
	const auto maxStreamSize	= std::numeric_limits<std::streamsize>::max();

	double currentX, accurateFuncVal, funcVal, absoluteError, relativeError;

	//naglowek pliku wyjsciowego
	outputFile 
		<< std::setw(formatWidth) << 'x'					<< " |"
		<< std::setw(formatWidth) << "accurate f(x)"		<< " |"
		<< std::setw(formatWidth) << "f(x) using exp()"		<< " |"
		<< std::setw(formatWidth) << "abs. error exp"		<< " |"
		<< std::setw(formatWidth) << "f(x) using Taylor"	<< " |"
		<< std::setw(formatWidth) << "abs. error Taylor"	<< '\n';

	while (!inputFile.eof() && !inputFile.bad()) {
		//ignoruj pierwsza kolumne
		inputFile.ignore(maxStreamSize, ' ');

		inputFile >> currentX;
		inputFile >> accurateFuncVal;

		//liczenie wyniku dla funkcji z exp()
		funcVal = f(currentX);
		relativeError = abs(accurateFuncVal - funcVal);
		absoluteError = abs(relativeError / accurateFuncVal);

		outputFile
			<< std::setprecision(formatPrecision)
			<< std::scientific
			<< currentX			<< ' '
			<< accurateFuncVal	<< ' '
			<< funcVal			<< ' '
			<< absoluteError	<< ' ';

		//liczenie wyniku dla funkcji za pomoca szeregu taylora
		funcVal = taylorSeries(currentX);
		relativeError = abs(accurateFuncVal - funcVal);
		absoluteError = abs(relativeError / accurateFuncVal);

		outputFile
			<< std::setprecision(formatPrecision)
			<< std::scientific
			<< funcVal			<< ' '
			<< absoluteError	<< '\n';

		//ignoruj biale znaki na koncu lini
		inputFile.ignore(maxStreamSize, '\n');
	}


	inputFile.close();
	outputFile.close();
	return 0;
}