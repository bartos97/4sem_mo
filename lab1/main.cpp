#include <iostream>

template<typename T>
unsigned int mantissaCounter(T* resultPtr) {
    unsigned int counter=0;
    T epsilon, tmpVal;
    tmpVal = epsilon = 1.0;

    while (T(1.0) + (tmpVal /= T(2.0)) > T(1.0)) {
        epsilon = tmpVal;
        counter++;
    }

    *resultPtr = epsilon;
    return counter;
}


int main() {
    float epsilonFloat;
    std::cout << "Liczba bitow mantysy typu float: " << mantissaCounter<float>(&epsilonFloat);
    std::cout << "\nEpsilon typu float: " << epsilonFloat << "\n";

    double epsilonDouble;
    std::cout << "\nLiczba bitow mantysy typu double: " << mantissaCounter<double>(&epsilonDouble);
    std::cout << "\nEpsilon typu double: " << epsilonDouble << std::endl;

    return 0;
}
