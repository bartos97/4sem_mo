#define _USE_MATH_DEFINES

#include <cmath>
#include <iostream>
#include <fstream>
#include <clocale>
#include <iomanip>

#include "MatrixVectorUtilities.h"
    

inline double AnalyticSolution(const double t)
{
    return 1.0 - std::exp(-10.0 * (t + atan(t)));
}


inline double DiffEquFunction_f_of_t(const double t)
{
    return (10.0 * t * t + 20.0) / (t * t + 1);
}


void CreateNodes(double* nodes, const double h, const size_t n)
{
    for (size_t i = 0; i < n; i++)
    {
        nodes[i] = h * i;
    }
}


double ErrorDirectEulerMethod(double* nodes, const double h, const size_t n)
{
    double normMax = 0.0;
    double error = 0.0;
    double y = 0.0;

    for (int i = 1; i < n; i++)
    {
        y = y - h * DiffEquFunction_f_of_t(nodes[i]) * (y - 1);

        error = std::abs(AnalyticSolution(nodes[i]) - y);
        if (error > normMax)
            normMax = error;
    }

    return normMax;
}


double ErrorTrapeziumMethod(double* nodes, const double h, const size_t n)
{
    double normMax = 0.0;
    double y = 0.0;
    double error;
    double tmpK0;
    double tmpK1;

    for (int i = 1; i < n; i++)
    {
        tmpK0 = DiffEquFunction_f_of_t(nodes[i]) * h;
        tmpK1 = DiffEquFunction_f_of_t(nodes[i + 1]) * h;
        y = (2.0 * y - tmpK0 * (y - 1.0) + tmpK1) / (2.0 + tmpK1);

        error = std::abs(AnalyticSolution(nodes[i]) - y);
        if (error > normMax)
            normMax = error;
    }

    return normMax;
}


double ErrorIndirectEulerMethod(double* nodes, const double h, const size_t n)
{
    double normMax = 0.0;
    double y = 0.0;
    double error;
    double tmp;

    for (int i = 1; i < n; i++)
    {
        tmp = DiffEquFunction_f_of_t(nodes[i]) * h;
        y = (y + tmp) / (1.0 + tmp);

        error = std::abs(AnalyticSolution(nodes[i]) - y);
        if (error > normMax)
            normMax = error;
    }

    return normMax;
}


void DirectEulerMethod(double* y, const double* nodes, const double h, const size_t n)
{
    y[0] = 0.0;

    for (int i = 1; i < n; i++)
    {
        y[i] = y[i - 1] - h * DiffEquFunction_f_of_t(nodes[i]) * (y[i - 1] - 1);
    }
}


void IndirectEulerMethod(double* y, const double* nodes, const double h, const size_t n)
{
    y[0] = 0.0;
    double tmp;

    for (int i = 1; i < n; i++)
    {
        tmp = DiffEquFunction_f_of_t(nodes[i]) * h;
        y[i] = (y[i - 1] + tmp) / (1.0 + tmp);
    }
}


void TrapeziumMethod(double* y, const double* nodes, const double h, const size_t n)
{
    y[0] = 0.0;
    double tmpK0;
    double tmpK1;

    for (int i = 1; i < n; i++)
    {
        tmpK0 = DiffEquFunction_f_of_t(nodes[i]) * h;
        tmpK1 = DiffEquFunction_f_of_t(nodes[i + 1]) * h;
        y[i] = (2.0 * y[i - 1] - tmpK0 * (y[i - 1] - 1.0) + tmpK1) / (2.0 + tmpK1);
    }
}


void ChartsIndirectAndTrapezium(const double deltaT = 0.01, const double tStart = 0.0, const double tEnd = 3.0)
{
    const int numOfNodes = int((tEnd - tStart) / deltaT);

    auto* resultIndirectEuler = new double[numOfNodes];
    auto* resultTrapezium = new double[numOfNodes];
    auto* nodes = new double[numOfNodes];

    for (int i = 0; i < numOfNodes; i++)
    {
        nodes[i] = tStart + i * deltaT;
    }

    IndirectEulerMethod(resultIndirectEuler, nodes, deltaT, numOfNodes);
    TrapeziumMethod(resultTrapezium, nodes, deltaT, numOfNodes);

    const char* fileName = "results_indirect_trapezium.csv";
    std::ofstream file;
    file.exceptions(std::ofstream::badbit);

    try
    {
        file.open(fileName, std::ofstream::out | std::ofstream::trunc);
        file << "t;analytic;indirect;trapezium\n";

        for (int i = 0; i < numOfNodes; i++)
        {
            file << std::fixed
                 << nodes[i] << ';'
                 << AnalyticSolution(nodes[i]) << ';'
                 << resultIndirectEuler[i] << ';'
                 << resultTrapezium[i] << '\n';

        }
        
        file.close();
    }
    catch (const std::ofstream::failure& e)
    {
        std::cerr << "Error when opening / reading file " << fileName << ":" << e.what() << std::endl;
    }

    delete[] resultIndirectEuler;
    delete[] resultTrapezium;
    delete[] nodes;
}


void ChartDirect(const double deltaT = 0.01, const double tStart = 0.0, const double tEnd = 3.0)
{
    const int numOfNodes = int((tEnd - tStart) / deltaT);
    std::vector<double> resultDirectEuler;
    std::vector<double> nodes;
    resultDirectEuler.resize(numOfNodes);
    nodes.resize(numOfNodes);

    for (int i = 0; i < numOfNodes; i++)
    {
        nodes[i] = tStart + i * deltaT;
    }

    DirectEulerMethod(resultDirectEuler.data(), nodes.data(), deltaT, numOfNodes);

    std::string fileName = "results_direct_stable.csv";
    std::ofstream file;
    file.exceptions(std::ofstream::badbit);

    try
    {
        file.open(fileName, std::ofstream::out | std::ofstream::trunc);
        file << "t;analytic;direct_stable\n";

        for (int i = 0; i < numOfNodes; i++)
        {
            file << std::fixed
                 << nodes[i] << ';'
                 << AnalyticSolution(nodes[i]) << ';'
                 << resultDirectEuler[i] << '\n';
        }

        file.close();
    }
    catch (const std::ofstream::failure& e)
    {
        std::cerr << "Error when opening / reading file " << fileName << ":" << e.what() << std::endl;
    }

    //niestabilnosc
    const double biggestStableDelta = (tEnd * tEnd + 1.0) / (5 * (tEnd * tEnd + 2.0));
    const double unstableDeltaT = deltaT + biggestStableDelta;
    const int unstableNumOfNodes = int((tEnd - tStart) / unstableDeltaT);

    resultDirectEuler.resize(unstableNumOfNodes);
    nodes.resize(unstableNumOfNodes);

    for (int i = 0; i < unstableNumOfNodes; i++)
    {
        nodes[i] = tStart + i * unstableDeltaT;
        resultDirectEuler[i] = 0.0;
    }

    DirectEulerMethod(resultDirectEuler.data(), nodes.data(), unstableDeltaT, unstableNumOfNodes);

    fileName = "results_direct_unstable.csv";
    std::ofstream file2;
    file2.exceptions(std::ofstream::badbit);
    try
    {
        file2.open(fileName, std::ofstream::out | std::ofstream::trunc);
        file2 << "t;analytic;direct_unstable;\n";

        for (int i = 0; i < unstableNumOfNodes; i++)
        {
            file2 << std::fixed
                  << nodes[i] << ';'
                  << AnalyticSolution(nodes[i]) << ';'
                  << resultDirectEuler[i] << '\n';
        }

        file2.close();
    }
    catch (const std::ofstream::failure& e)
    {
        std::cerr << "Error when opening / reading file " << fileName << ":" << e.what() << std::endl;
    }
}


void ChartErrors(const double tStart = 0.0, const double tEnd = 3.0)
{
    //biggest stable delta
    double deltaT = (tEnd * tEnd + 1.0) / (5 * (tEnd * tEnd + 2.0));

    size_t numOfNodes = 0;
    std::vector<double> nodes;

    const std::string fileName = "results_errors.csv";
    std::ofstream file;
    file.exceptions(std::ofstream::badbit);

    try
    {
        file.open(fileName, std::ofstream::out | std::ofstream::trunc);
        file << "deltaT;error_direct;error_indirect;error_trapez\n";

        while (deltaT > DBL_EPSILON)
        {
            numOfNodes = size_t((tEnd - tStart) / deltaT);
            if (numOfNodes > 100000)
                numOfNodes = 100000;

            nodes.resize(numOfNodes);
            auto nodesIt = nodes.begin();
            for (size_t i = 0; i < numOfNodes; i++)
            {
                *(nodesIt++) = tStart + i * deltaT;
            }

            file << std::fixed
                 << std::log10(deltaT) << ';'
                 << std::log10(ErrorDirectEulerMethod(nodes.data(), deltaT, numOfNodes)) << ';'
                 << std::log10(ErrorIndirectEulerMethod(nodes.data(), deltaT, numOfNodes)) << ';'
                 << std::log10(ErrorTrapeziumMethod(nodes.data(), deltaT, numOfNodes)) << '\n';

            deltaT /= 2.0;
        }

        file.close();
    }
    catch (const std::ofstream::failure& e)
    {
        std::cerr << "Error when opening / reading file " << fileName << ":" << e.what() << std::endl;
    }
}


int main()
{
    //zeby w liczbach dzisietnych miec przecinki
    //auto loc = std::locale("");
    //std::setlocale(LC_ALL, "pl_PL.UTF-8");
    //std::locale::global(loc);

    ChartsIndirectAndTrapezium();
    ChartDirect();
    ChartErrors();

    return 0;

}
