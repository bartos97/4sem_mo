#pragma once
#include <functional>
#include <cmath>

#define DEFAULT_STEP 1.0/2.0

template<typename FloatType>
class DerivativeCalculator
{
public:
    DerivativeCalculator(const std::function<FloatType(FloatType)>& function)
        : m_function(function)
    {}
    

    FloatType calculate(FloatType x, FloatType step = DEFAULT_STEP)
    {
        bool flagBackward = true;
        bool flagProgressive = true;
        FloatType backwardValue = m_function(x - step);
        FloatType progressiveValue = m_function(x + step);

        if (std::isnan(backwardValue) || std::isinf(backwardValue))
            flagBackward = false;
        if (std::isnan(progressiveValue) || std::isinf(progressiveValue))
            flagProgressive = false;

        if (flagBackward && flagProgressive)
            return centralDifference(x, step);
        if (!flagBackward)
            return progressiveDifference3P(x, step);
        if (!flagProgressive)
            return backwardDifference3P(x, step);

        return NAN;
    }
    

    inline FloatType centralDifference(FloatType x, FloatType step = DEFAULT_STEP)
    {
        return  (m_function(x + step) - m_function(x - step)) / FloatType(2.0) * step;
    }
    

    inline FloatType progressiveDifference2P(FloatType x, FloatType step = DEFAULT_STEP)
    {
        return (m_function(x + step) - m_function(x)) / step;
    }
    

    inline FloatType progressiveDifference3P(FloatType x, FloatType step = DEFAULT_STEP)
    {
        FloatType numerator = (-1.5) * m_function(x) 
                           + 2.0 * m_function(x + step) 
                           - 0.5 * m_function(x + 2 * step);
        return numerator / step;
    }
    

    inline FloatType backwardDifference2P(FloatType x, FloatType step = DEFAULT_STEP)
    {
        return (m_function(x) - m_function(x - step)) / step;
    }
    

    inline FloatType backwardDifference3P(FloatType x, FloatType step = DEFAULT_STEP)
    {
        FloatType numerator = 0.5 * m_function(x - 2 * step) 
                           - 2.0 * m_function(x - step) 
                           + 1.5 * m_function(x);
        return numerator / step;
    }


private:
    std::function<FloatType(FloatType)> m_function;
};

