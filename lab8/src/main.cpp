#include <iostream>
#include <vector>
#include "../matplotlib-cpp/matplotlibcpp.h"
#include "DerivativeCalculator.h"


#define SCALE_LOG
namespace plt = matplotlibcpp;


template<typename T>
T getMachineEpsilon()
{
    T epsilon, tmpVal;
    tmpVal = epsilon = 1.0;

    while (T(1.0) + (tmpVal /= T(2.0)) > T(1.0))
    {
        epsilon = tmpVal;
    }

    return epsilon;
}


template<typename FloatType>
inline FloatType function(FloatType x)
{
    return std::cos(x / 2);
}


template<typename FloatType>
inline FloatType derivative(FloatType x)
{
    return (-0.5) * std::sin(x / 2);
}


template<typename FloatType>
size_t generateSteps(std::vector<FloatType>& stepsVector)
{
    FloatType epsilon = getMachineEpsilon<FloatType>();
    FloatType step = 0.1;
    size_t i = 0;

    while (step > epsilon)
    {
        i++;
        step /= 10.0;
        stepsVector.push_back(step);
    }

    return i;
}


template<typename FloatType>
inline void plot(const std::string& title,
                 const std::string& xlabel,
                 const std::string& ylabel,
                 const std::vector<FloatType>& x, 
                 const std::vector<FloatType>& y, 
                 bool openWindow)
{
    plt::plot(x, y1, "-r");
    plt::title(title);
    plt::xlabel(xlabel);
    plt::ylabel(ylabel);
    if (openWindow) plt::show();
    else plt::save("./out/" + title + ".png");
    plt::clf();
}


template<typename FloatType>
inline void plot2(const std::string& title,
                  const std::string& xlabel,
                  const std::string& ylabel,
                  const std::vector<FloatType>& x,
                  const std::vector<FloatType>& y1,
                  const std::vector<FloatType>& y2,
                  bool openWindow)
{
    plt::plot(x, y1, "-r");
    plt::plot(x, y2, "--g");
    plt::title(title);
    plt::xlabel(xlabel);
    plt::ylabel(ylabel);
    if (openWindow) plt::show();
    else plt::save("./out/" + title + ".png");
    plt::clf();
}


template<typename FloatType>
inline void plot3(const std::string& title,
                  const std::string& xlabel,
                  const std::string& ylabel,
                  const std::vector<FloatType>& x,
                  const std::vector<FloatType>& y1,
                  const std::vector<FloatType>& y2,
                  const std::vector<FloatType>& y3,
                  bool openWindow)
{
    plt::plot(x, y1, "-r");
    plt::plot(x, y2, "--g");
    plt::plot(x, y3, "x-m");
    plt::title(title);
    plt::xlabel(xlabel);
    plt::ylabel(ylabel);
    if (openWindow) plt::show();
    else plt::save("./out/" + title + ".png");
    plt::clf();
}


template<typename FloatType>
void doTheJob(FloatType left, FloatType center, FloatType right, bool openWindow = true)
{
    DerivativeCalculator<FloatType> derivCalc(function<FloatType>);

    std::vector<FloatType> steps;
    auto stepsAmount = generateSteps<FloatType>(steps);

    //Containers setup

    std::vector<FloatType> x; //x axis
    x.reserve(stepsAmount);

    std::vector<FloatType> plotProg2P[2]; //left and center
    for (auto& elem : plotProg2P)
        elem.reserve(stepsAmount);

    std::vector<FloatType> plotProg3P[2]; //left and center
    for (auto& elem : plotProg3P)
        elem.reserve(stepsAmount);

    std::vector<FloatType> plotBack2P[2]; //center and right
    for (auto& elem : plotBack2P)
        elem.reserve(stepsAmount);

    std::vector<FloatType> plotBack3P[2]; //center and right
    for (auto& elem : plotBack3P)
        elem.reserve(stepsAmount);

    std::vector<FloatType> plotCentral; //only center
    plotCentral.reserve(stepsAmount);

    std::vector<FloatType> eps(stepsAmount, std::log10(getMachineEpsilon<FloatType>()));

    //Data preparation

    FloatType derivLeft = derivative<FloatType>(left);
    FloatType derivCenter = derivative<FloatType>(center);
    FloatType derivRight = derivative<FloatType>(right);

#ifdef SCALE_LOG
    for (auto& step : steps)
    {
        x.push_back(std::log10(step));

        //left
        plotProg2P[0].push_back(std::log10(std::abs(
            derivLeft - derivCalc.progressiveDifference2P(left, step)
        )));
        plotProg3P[0].push_back(std::log10(std::abs(
            derivLeft - derivCalc.progressiveDifference3P(left, step)
        )));

        //center
        plotProg2P[1].push_back(std::log10(std::abs(
            derivCenter - derivCalc.progressiveDifference2P(center, step)
        )));
        plotProg3P[1].push_back(std::log10(std::abs(
            derivCenter - derivCalc.progressiveDifference3P(center, step)
        )));
        plotCentral.push_back(std::log10(std::abs(
            derivCenter - derivCalc.centralDifference(center, step)
        )));
        plotBack2P[0].push_back(std::log10(std::abs(
            derivCenter - derivCalc.backwardDifference2P(center, step)
        )));
        plotBack3P[0].push_back(std::log10(std::abs(
            derivCenter - derivCalc.backwardDifference3P(center, step)
        )));

        //right
        plotBack2P[1].push_back(std::log10(std::abs(
            derivRight - derivCalc.backwardDifference2P(right, step)
        )));
        plotBack3P[1].push_back(std::log10(std::abs(
            derivRight - derivCalc.backwardDifference3P(right, step)
        )));
    }

#else

    for (auto& step : steps)
    {
        x.push_back(step);

        //left
        plotProg2P[0].push_back(std::abs(
            derivative<FloatType>(left) - derivCalc.progressiveDifference2P(left, step)
        ));
        plotProg3P[0].push_back(std::abs(
            derivative<FloatType>(left) - derivCalc.progressiveDifference3P(left, step)
        ));

        //center
        plotProg2P[1].push_back(std::abs(
            derivative<FloatType>(center) - derivCalc.progressiveDifference2P(center, step)
        ));
        plotProg3P[1].push_back(std::abs(
            derivative<FloatType>(center) - derivCalc.progressiveDifference3P(center, step)
        ));
        plotCentral.push_back(std::abs(
            derivative<FloatType>(center) - derivCalc.centralDifference(center, step)
        ));
        plotBack2P[0].push_back(std::abs(
            derivative<FloatType>(center) - derivCalc.backwardDifference2P(center, step)
        ));
        plotBack3P[0].push_back(std::abs(
            derivative<FloatType>(center) - derivCalc.backwardDifference3P(center, step)
        ));

        //right
        plotBack2P[1].push_back(std::abs(
            derivative<FloatType>(right) - derivCalc.backwardDifference2P(right, step)
        ));
        plotBack3P[1].push_back(std::abs(
            derivative<FloatType>(right) - derivCalc.backwardDifference3P(right, step)
        ));
    }
#endif // SCALE_LOG

    //Wykresy

#ifdef SCALE_LOG
    std::string xlabel = "log10(h)";
#else
    std::string xlabel = "h";
#endif // SCALE_LOG
    std::string ylabel = "Blad bezwzgledny";

    std::string type, title;
    type = sizeof(double) > sizeof(FloatType) ? "float" : "double";

    //WYKRES: progresywna 2-punktowa
    title = type + " - progresywna 2-punktowa";
    plot3(title, xlabel, ylabel, x, plotProg2P[0], plotProg2P[1], eps, openWindow);    

    //WYKRES: progresywna 3-punktowa
    title = type + " - progresywna 3-punktowa";
    plot3(title, xlabel, ylabel, x, plotProg3P[0], plotProg3P[1], eps, openWindow);

    //WYKRES: centralna
    title = type + " - centralna";
    plot2(title, xlabel, ylabel, x, plotCentral, eps, openWindow);

    //WYKRES: wsteczna 2-punktowa
    title = type + " - wsteczna 2-punktowa";
    plot3(title, xlabel, ylabel, x, plotBack2P[0], plotBack2P[1], eps, openWindow);

    //WYKRES: wsteczna 3-punktowa
    title = type + " - wsteczna 3-punktowa";
    plot3(title, xlabel, ylabel, x, plotBack3P[0], plotBack3P[1], eps, openWindow);
}


int main()
{
    plt::backend("WXAgg");
    doTheJob<float>(0.1f, float(M_PI / 2.0), float(M_PI), true);
    //doTheJob<double>(0.1, M_PI / 2.0, M_PI, false);
    return 0;
}
