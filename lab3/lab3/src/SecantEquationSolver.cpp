#include "SecantEquationSolver.h"
#include <algorithm>


SecantEquationSolver::SecantEquationSolver(unsigned int maxIterations, 
                                           double tolerance, 
                                           double startX0, 
                                           double startX1, 
                                           double(*functionToSolve)(double))
    : EquationSolver(maxIterations, tolerance, startX0, functionToSolve), m_startingPoint1(startX1)
{
    if (m_function(m_startingPoint1) > m_function(m_startingPoint))
    {
        std::swap<double>(m_startingPoint, m_startingPoint1);
    }
}


double SecantEquationSolver::calculate()
{
#ifdef ES_PRINT_STEPS
    printf(
        "------------------------------\n"
        "------ METODA SIECZNYCH ------\n"
        "------------------------------\n"
    );
#endif // ES_PRINT_STEPS

    unsigned int i = 0;
    double errorEstimator, residuum, tmp;

    double x1 = m_startingPoint; //Xn
    double y1 = m_function(x1);

    double x2 = m_startingPoint1; //Xn+1
    double y2 = m_function(x2);

    do
    {
        i++;

        tmp = x2 - y2 / ((y2 - y1) / (x2 - x1)); //to bedzie Xn+2
        errorEstimator = fabs(tmp - x2); //|En| = |Xn+2 - Xn+1|

        x1 = x2;
        y1 = y2;

        x2 = tmp;
        y2 = m_function(x2);
        residuum = fabs(y2);

    #ifdef ES_PRINT_STEPS
        printf(ES_PRINTF_FORMAT, i, x2, errorEstimator, residuum);
    #endif // ES_PRINT_STEPS

    } while (i < MAX_ITERATIONS && residuum > TOLERANCE && errorEstimator > TOLERANCE);

    return x2;
}
