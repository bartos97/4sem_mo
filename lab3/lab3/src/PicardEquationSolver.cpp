#include "PicardEquationSolver.h"


PicardEquationSolver::PicardEquationSolver(const unsigned int maxIterations, 
                                           double tolerance, 
                                           double start,
                                           double(*methodDefineFunction)(double))
    : EquationSolver(maxIterations, tolerance, start, methodDefineFunction)
{}


double PicardEquationSolver::calculate()
{
#ifdef ES_PRINT_STEPS
    printf(
        "----------------------------\n"
        "------ METODA PICARDA ------\n"
        "----------------------------\n"
    );
#endif // ES_PRINT_STEPS

    unsigned int i = 0;
    double prevValue = m_startingPoint;
    double currentValue = prevValue;
    double errorEstimator, residuum;

    do
    {
        i++;
        prevValue = currentValue;
        currentValue = m_function(prevValue);
        errorEstimator = fabs(currentValue - prevValue);
        residuum = fabs(currentValue);

    #ifdef ES_PRINT_STEPS
        printf(ES_PRINTF_FORMAT, i, prevValue, errorEstimator, residuum);
    #endif // ES_PRINT_STEPS

    } while (i < MAX_ITERATIONS && residuum > TOLERANCE && errorEstimator > TOLERANCE);

    return prevValue;
}