#pragma once
#include <math.h>
#include <iostream>

// ifdef to beda wypisywane posrednie rezultaty obliczen
// ifndef to tylko zwracany wynik
#define ES_PRINT_STEPS
#define ES_PRINTF_FORMAT "i = %-5d Xn = %-30.20lf En = %-30.20lf |f(Xn)| = %-30.20lf \n"



/**
 * Klasa bazowa dla wszystkich klas implementujacych konkretne metody
 * rozwiazania pojedynczego algebraicznego rownania nieliniowego
 */
class EquationSolver
{
public:
    virtual double calculate() = 0;

protected:
    EquationSolver(unsigned int maxIterations, 
                   double tolerance, 
                   double start, 
                   double(*fun)(double))
        : MAX_ITERATIONS(maxIterations), TOLERANCE(tolerance), m_startingPoint(start), m_function(fun)
    {}

    double(*m_function)(double);

    const unsigned int MAX_ITERATIONS;
    const double TOLERANCE;
    double m_startingPoint;
};