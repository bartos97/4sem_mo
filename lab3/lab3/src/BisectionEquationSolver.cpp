#include "BisectionEquationSolver.h"

BisectionEquationSolver::BisectionEquationSolver(unsigned int maxIterations,
                                                 double tolerance,
                                                 double startA,
                                                 double startB,
                                                 double(*functionToSolve)(double))
    : EquationSolver(maxIterations, tolerance, startA, functionToSolve), m_startingPointB(startB)
{
    if (signum(m_function(m_startingPoint)) == signum(m_function(m_startingPointB)))
    {
        std::cerr << "Nalezy podac przedzial poczatkowy taki, ze sgn(a) != sgn(b)" << std::endl;
        m_isValid = false;
    }
    else m_isValid = true;
}


double BisectionEquationSolver::calculate()
{
    if (!m_isValid)
    {
        std::cerr << "Podano niepoprawny przedzial poczatkowy" << std::endl;
        return 0.0;
    }

#ifdef ES_PRINT_STEPS
    printf(
        "-----------------------------\n"
        "------ METODA BISEKCJI ------\n"
        "-----------------------------\n"
    );
#endif // ES_PRINT_STEPS

    double a, b;
    if (m_startingPoint > m_startingPointB)
    {
        a = m_startingPointB;
        b = m_startingPoint;
    }
    else
    {
        a = m_startingPoint;
        b = m_startingPointB;
    }

    double errorEstimator = b - a; //tak na prawde to dopiero nim bedzie
    double valAtA = m_function(a);
    double valAtB = m_function(b);
    double  residuum, middle;
    unsigned int i = 0;

    do
    {
        i++;
        errorEstimator /= 2; //legitny estymator: polowa dlugosci przedzialu
        middle = a + errorEstimator; //srodek przedzialu
        residuum = m_function(middle);

    #ifdef ES_PRINT_STEPS
        printf(ES_PRINTF_FORMAT, i, middle, errorEstimator, residuum);
    #endif // ES_PRINT_STEPS

        if (signum(valAtA) != signum(residuum))
        {
            b = middle;
            valAtB = residuum;
        }
        else
        {
            a = middle;
            valAtA = residuum;
        }
    } while (i < MAX_ITERATIONS && residuum > TOLERANCE && errorEstimator > TOLERANCE);

    return middle;
}


inline int BisectionEquationSolver::signum(double x)
{
    return (0.0 < x) - (0.0 > x);
}