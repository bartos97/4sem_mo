#pragma once
#include "EquationSolver.h"


class NewtonEquationSolver :
    public EquationSolver
{
public:
    /**
     * @param maxIterations - kryterium zakonczenia, arbitralne ograniczenie ilosci iteracji
     * @param tolerance - zadana tolerancja bledu,
     * ta sama dla wyznaczenia pierwiastka jak i jego wiarygodnosci
     * @param startPoint - punkt poczatkowy iteracji
     * @param functionToSolve - funkcja ktorej pierwiastek bedzie liczony
     * @param functionsDerivative - pochodna functionToSolve
     */
    NewtonEquationSolver(unsigned int maxIterations,
                         double tolerance,
                         double startPoint,
                         double(*functionToSolve)(double),
                         double(*functionsDerivative)(double));

    virtual double calculate() override;

private:
    double(*m_derivative)(double);
};

