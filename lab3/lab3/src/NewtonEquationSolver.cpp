#include "NewtonEquationSolver.h"


NewtonEquationSolver::NewtonEquationSolver(unsigned int maxIterations, 
                                           double tolerance, 
                                           double startPoint, 
                                           double(*functionToSolve)(double), 
                                           double(*functionsDerivative)(double))
    : EquationSolver(maxIterations, tolerance, startPoint, functionToSolve), m_derivative(functionsDerivative)
{}


double NewtonEquationSolver::calculate()
{
#ifdef ES_PRINT_STEPS
    printf(
        "----------------------------\n"
        "------ METODA NEWTONA ------\n"
        "----------------------------\n"
    );
#endif // ES_PRINT_STEPS

    unsigned int i = 0;
    double errorEstimator, residuum;
    double x = m_startingPoint;
    double y = m_function(x);

    do
    {
        i++;
        errorEstimator = -x;

        x = x - y / m_derivative(x); //Xn+1, czyli Fi(x)
        errorEstimator = fabs(errorEstimator + x); //|En| = |-Xn + Xn+1|

        y = m_function(x); //f(Xn+1)
        residuum = fabs(y);

    #ifdef ES_PRINT_STEPS
        printf(ES_PRINTF_FORMAT, i, x, errorEstimator, residuum);
    #endif // ES_PRINT_STEPS

    } while (i < MAX_ITERATIONS && residuum > TOLERANCE && errorEstimator > TOLERANCE);

    return x;
}