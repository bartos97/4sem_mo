#pragma once
#include "EquationSolver.h"


class BisectionEquationSolver :
    public EquationSolver
{
public:
    /**
     * @param maxIterations - kryterium zakonczenia, arbitralne ograniczenie ilosci iteracji
     * @param tolerance - zadana tolerancja bledu,
     * ta sama dla wyznaczenia pierwiastka jak i jego wiarygodnosci
     * @param startA - poczatek przedzialu startowego
     * @param startB - koniec przedzialu startowego
     * @param functionToSolve - funkcja ktorej pierwiastek bedzie liczony
     */
    BisectionEquationSolver(unsigned int maxIterations,
                            double tolerance,
                            double startA,
                            double startB,
                            double(*functionToSolve)(double));

    virtual double calculate() override;

private:
    inline int signum(double x);

    double m_startingPointB;
    bool m_isValid;
};

