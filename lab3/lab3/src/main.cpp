#include <iostream>
#include <math.h>
#include "PicardEquationSolver.h"
#include "BisectionEquationSolver.h"
#include "NewtonEquationSolver.h"
#include "SecantEquationSolver.h"


template<typename T>
T getMachineEpsilon()
{
    T epsilon, tmpVal;
    tmpVal = epsilon = 1.0;

    while (T(1.0) + (tmpVal /= T(2.0)) > T(1.0))
    {
        epsilon = tmpVal;
    }

    return epsilon;
}


//f(x) = sin^2(x/4) - x
inline double fun1(double x)
{
    double sine = sin(x / 4);
    return sine * sine - x;
}

// f'(x) = 1/4 * sin(x/2) - 1
inline double fun1derivative(double x)
{
    return 0.25 * sin(x / 2) - 1;
}

//f(x) = tan(2x) - x - 1
inline double fun2(double x)
{
    return tan(2 * x) - x - 1;
}

//f'(x) = 2 / cos^2(2x) - 1
inline double fun2derivative(double x)
{
    double cosine = cos(2 * x);
    return 2 / (cosine*cosine) - 1;
}

//f(x) = 2x^3 - 8x + 5
inline double funTest(double x)
{
    return x * (-8 + 2 * x*x) + 5; 
}

//f'(x) = 6x^2 - 8
inline double funTestDerivative(double x)
{
    return 6 * x*x - 8;
}


int main()
{
    double epsilon = getMachineEpsilon<double>();
    double result;

    PicardEquationSolver picard = PicardEquationSolver(100, epsilon, 75.0, [](double x) -> double {
        double sine = sin(x / 4);
        return sine * sine;
    });
    result = picard.calculate();
    std::cout << "\nMetoda Picarda wynik: " << result << "\n" << std::endl;


    BisectionEquationSolver bisection = BisectionEquationSolver(100, epsilon, -1.125, 3.0, fun1);
    result = bisection.calculate();
    std::cout << "\nMetoda bisekcji wynik: " << result << "\n" << std::endl;


    NewtonEquationSolver newton = NewtonEquationSolver(100, epsilon, 2.0, fun1, fun1derivative);
    result = newton.calculate();
    std::cout << "\nMetoda Newtona wynik: " << result << "\n" << std::endl;


    SecantEquationSolver secant = SecantEquationSolver(100, epsilon, 3.0, 2.5, fun1);
    result = secant.calculate();
    std::cout << "\nMetoda siecznych wynik: " << result << "\n" << std::endl;

    return 0;
}