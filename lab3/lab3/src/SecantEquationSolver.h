#pragma once
#include "EquationSolver.h"


class SecantEquationSolver :
    public EquationSolver
{
public:
    /**
     * @param maxIterations - kryterium zakonczenia, arbitralne ograniczenie ilosci iteracji
     * @param tolerance - zadana tolerancja bledu,
     * ta sama dla wyznaczenia pierwiastka jak i jego wiarygodnosci
     * @param startX0 - punkt poczatkowy
     * @param startX1 - kolejne przyblizenie pierwiastka 
     * (jesli gorsze niz startX0 to zostana zamieione miejscami)
     * @param functionToSolve - funkcja ktorej pierwiastek bedzie liczony
     */
    SecantEquationSolver(unsigned int maxIterations,
                         double tolerance,
                         double startX0,
                         double startX1,
                         double(*functionToSolve)(double));

    virtual double calculate() override;

private:
    double m_startingPoint1;
};

