#pragma once
#include "EquationSolver.h"


class PicardEquationSolver :
    public EquationSolver
{
public:
    /**
     * @param maxIterations - kryterium zakonczenia, arbitralne ograniczenie ilosci iteracji
     * @param tolerance - zadana tolerancja bledu,
     * ta sama dla wyznaczenia pierwiastka jak i jego wiarygodnosci
     * @param start - punkt poczatkowy z ktorego zaczac metode iteracyjna
     * @param methodDefineFunction - funkcja definiujaca metode Picarda: Fi(x) = x
     * w przypadku tej metody trzeba ja obliczyc analitycznie i podac tutaj
     */
    PicardEquationSolver(unsigned int maxIterations, 
                         double tolerance, 
                         double start,
                         double (*methodDefineFunction)(double));

    virtual double calculate() override;
};

